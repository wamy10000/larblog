<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table = 'article';
	protected $fillable = ['cate_id','user_id','title', 'content', 'keyword','description','click','status','published_at','del'];
	//获得带标签的文章
    public function tags()
    {
		return $this->belongsToMany('App\Tag','article_tag','art_id','tag_id');
	}
	//评论
	public function comments()
	{
		return $this->hasMany('App\Comment','art_id');
	} 
	//关联作者
	public function user()
	{
		return $this->belongsTo('App\User');
	}
	//关联栏目
	public function cate()
	{
		return $this->belongsTo('App\Category','cate_id');
	}
	//发布时间倒序排列没删除的文章
	public function scopeNoDel($query)
	{
		return $query->where('del',0)->orderBy('status','desc')->orderBy('published_at', 'desc');
	}
	//发布时间倒序排列删除的文章
	public function scopeDel($query)
	{
		return $query->where('del',1)->orderBy('published_at', 'desc');
	}
	//推荐文章
	public function scopeRecommend($query)
	{
		return $query->where(['status'=>1,'del'=>0])->orderBy('published_at','desc')->take(5);
	}
	//最新发布文章
	public function scopeNewArticle($query)
	{
		return $query->where('del',0)->orderBy('published_at', 'desc')->take(5);
	}
	//下一篇
	public function scopeNextArticle($query,$list,$id)
	{
		return $query->whereRaw('cate_id ='.$list.' and id >'.$id)->select('title','id')->orderBy('id','asc');
	}
	//上一篇
	public function scopePrevArticle($query,$list,$id)
	{
		return $query->whereRaw('cate_id ='.$list.' and id <'.$id)->select('title','id')->orderBy('id','desc');
	}
	//相关文章
	public function scopeSameArticle($query,$list)
	{
		return $query->where('cate_id',$list)->select('title','id')->orderBy('id','desc')->take(5);
	}




}
