<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    public $timestamps = false;	//不管理created_at和updated_at

    protected $hidden = ['created_at','updated_at'];	// 查询时隐藏着两个字段

    protected $guarded = [];

    public $html;
    
    //获取分类列表
    public static function getCateData(){
    	$cate = self::orderBy('sort','asc')->get();
    	$data = self::getSort($cate);
    	return $data;
    }
    //栏目关联文章
    public function catArticle(){
        return $this->hasMany('App\Article','cate_id');
    }
    //按分类排序
    public static function getSort($model,$pid=0,$level=0,$html='--'){
    	$data = array();
    	foreach($model as $k=>$v){
    		if($v->p_id == $pid){
    			if($level != 0){
    				$v->html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level);
    				$v->html .= '|';
    			}
    			$v->html .= str_repeat($html,$level);
    			$data[] = $v;
    			$data = array_merge($data,self::getSort($model,$v->id,$level+1));
    		}
    	}
    	return $data;
    }


}
