<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Comment, Auth;

class CommentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$info = Comment::getCommentData();

		return view('admin.comment', compact('info'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		if (!$request->contents)
		{
			return redirect()->back()->withError('提交内容不能为空');
		}

		$info = [
			'art_id'    => $request->art_id,
			'content'   => $request->contents,
			'user_id'   => Auth::user()->id,
			'parent_id' => $request->parent_id,
		];

		if (Auth::user()->id == 1)
		{
			$info['check'] = 1;
		}

		Comment::create($info);

		return redirect()->back()->withSuccess('发布成功,请等待管理员审核...');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Request $request
	 * @param  int $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$info = Comment::all();
		$info = Comment::getChilds($info, $id);
		foreach ($info as $v)
		{
			Comment::destroy($v['id']);
		}
		Comment::destroy($id);

		return redirect()->back()->withSuccess('成功删除该评论及相关回复');
	}

	public function setComment(Request $request)
	{
		$info = $request->except('_token');

		if (isset($info['ckb']))
		{
			Comment::whereIn('id', $info['ckb'])->update(['check' => 1]);

			return redirect()->back()->withSuccess('审核成功');
		}

		return redirect()->back()->withError('请先选择要审核的评论');
	}

}
