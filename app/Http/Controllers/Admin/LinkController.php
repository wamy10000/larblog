<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests,App\Http\Requests\LinkRequest;
use App\Http\Controllers\Controller;
use App\Link;
class LinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $links = Link::orderBy('created_at','desc')->get();

        return view('admin.link',compact('links'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(LinkRequest $request)
    {
        Link::create( $request->except('_token') );
        
        return redirect(url('yhsystem/link'))->withSuccess('添加成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,
            [
                'name'  =>  'required|min:3|unique:link,name,'.$id,
                'link'  =>  'required|url|unique:link,link,'.$id
            ]    //验证时忽略自身name,link
        );
        Link::findOrFail($id)->update($request->except(['_token','_method']));

        return redirect()->back()->withSuccess('修改成功');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Link::destroy($id);

        return redirect(url('yhsystem/link'))->withSuccess('删除成功');
    }
}
