<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CateController extends Controller
{

    /**
     * 栏目修改视图.
     *
     * @return Response
     */
    public function index()
    {
        $info = Category::getCateData();
        return view('admin.indexMenu')->withInfo($info);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   

    }

    /**
     * 栏目修改处理.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $info = $request->except('_token');
        Category::where('id',$info['id'])->update($info);
        return redirect(action('Admin\CateController@index'))->withSuccess('修改成功!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * 删除栏目.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $son = Category::where('p_id','=',$id)->get()->toArray();
        
        if(!empty($son)){
            return redirect(action('Admin\CateController@index'))->withError('请先删除下级分类');
        }
        
        if(Category::destroy($id)){
            return redirect(action('Admin\CateController@index'))->withSuccess('删除成功!');
        }
    }
    /**
     * 添加栏目处理
     * @param Request $request [description]
     */
    public function add(Request $request){
        $info = $request->except('_token');

        if(Category::insert($info)){
            return redirect(action('Admin\CateController@index'))->withSuccess('添加成功!');
        }
        
        return redirect(action('Admin\CateController@index'))->withError('添加栏目失败!');

    }
}
