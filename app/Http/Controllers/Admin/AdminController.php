<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Webset, App\User, App\Article, App\Comment, App\Talk;
use App\Http\Requests;
use App\Http\Controllers\Controller, Config;

class AdminController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		if (Auth()->user()->type !== 0) abort(404); //判断是否为管理员
	}

	//后台框架展示
	public function index()
	{
		return view('admin.admin');
	}

	//首页视图
	public function main()
	{
		$allArt = Article::where('del', 0)->count();
		$toArt = Article::where('published_at', '>=', date('Y-m-d', time()))->count();

		$allCom = Comment::count();
		$toCom = Comment::where('created_at', '>=', date('Y-m-d', time()))->count();

		$allTalk = Talk::count();
		$toTalk = Talk::where('created_at', '>=', date('Y-m-d', time()))->count();

		return view('admin.main', compact('allArt', 'toArt', 'allCom', 'toCom', 'allTalk', 'toTalk'));
	}

	//用户信息修改视图
	public function getUserInfo()
	{
		return view('admin.editAdmin');
	}

	//站点设置视图
	public function webSet()
	{
		$info = Webset::find(1)->toArray();

		return view('admin.site', $info);
	}

	//邮箱配置视图
	public function stemp()
	{
		return view('admin.smtp');
	}

	//用户管理视图
	public function myUser()
	{
		$users = User::where('type', 2)->paginate(5);

		return view('admin.myuser')->withMyusers($users);
	}

	//角色管理视图
	public function myRole()
	{
		$users = User::where('type', 0)->paginate(5);

		return view('admin.controlAdmin')->withMyusers($users);
	}

	//广告管理视图
	public function advert()
	{
		return view('admin.advert');
	}

	//友情链接视图
	public function link()
	{

	}


}
