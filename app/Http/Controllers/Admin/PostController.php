<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User,App\Webset,App\Category,Validator,Input;

class PostController extends Controller
{
    //修改用户信息
    public function getUserInfo(Request $request)
    {
    	$id = Auth()->user()->id;
        
        $this->validate($request,
            ['name' =>  'required|min:6|max:255|unique:users,name,'.$id]    //验证时忽略自身name
        );

        User::where('id',$id)->update($request->only('name','info'));

        return redirect(url('yhsystem/getUserInfo'))->withSuccess('资料修改成功');
    }
    //修改网站配置
    public function webSet(Request $request)
    {
    	if( Webset::where('id',1)->update($request->except('_token')) ){
    		return redirect(url('yhsystem/webSet'))->withSuccess('修改成功');
    	}

    	return redirect(url('yhsystem/webSet'))->withError('修改失败');
    }
    //修改邮箱设置
    public function stemp(Request $request)
    {
    	$data = $request->except('_token');
        //有相关信息提交，则修改.env中对应的配置项
        if($data){
            $this->mailSet($data);
            return redirect(url('yhsystem/stemp'))->withSuccess('修改成功');
        }
        return redirect(url('yhsystem/stemp'))->withError('修改失败');
    	
    }
    //本站用户管理
    public function myUser(Request $request)
    {
        $info = $request->except('_token');
        
        if( User::where('id',$info['id'])->update($request->only('status')) ){
            return redirect(url('yhsystem/myUser'));
        }
        
        dd('出错了');

    }
    //管理员管理
    public function myRole(Request $request)
    {
        $info = $request->except('_token');
        
        $this->validate($request,
            ['name' =>  'required|min:2|max:255|unique:users,name,'.$info['id']]
        );
        
        if( User::where('id',$info['id'])->update($request->only('name','info')) ){
            return redirect(url('yhsystem/myRole'))->withSuccess('修改成功');
        }
        
        dd('出错了');
    }
    //删除管理员
    public function delMyRole(Request $request)
    {
        $info = $request->except('_token');
        
        User::destroy($info['id']);
        
        return redirect(url('yhsystem/myRole'))->withSuccess('删除管理员成功');
    }
    //添加管理员
    public function register(Request $request)
    {
        $validator = Validator::make(Input::all(),User::$rules);

        if($validator->fails()){
            $error = $validator->errors();
            return redirect(url('yhsystem/myRole'))->withInput()->withErrors($validator->errors());
        }else{
            $input = $request->except('_token','password_confirmation');
            $input['password'] = bcrypt($input['password']);
            $input['type'] = 0; //管理员
            User::create($input);           
            return redirect(url('yhsystem/myRole'))->withSuccess('添加成功');
        }
    }
    //栏目排序
    public function sortCate(Request $request)
    {
        $info = $request->except('_token');
        
        foreach($info as $id=>$sort){
            Category::where('id',$id)->update(['sort'=>$sort]);
        }
        
        return redirect(url('yhsystem/cate'))->withSuccess('排序成功');
    }
    //通过修改env配置邮箱设置
    private function mailSet($data)
    {
        $origin_str = file_get_contents(base_path('.env'));

        $old = 'MAIL_HOST='.env('MAIL_HOST')."\nMAIL_PORT=".env('MAIL_PORT')."\nMAIL_USERNAME=".env('MAIL_USERNAME')."\nMAIL_PASSWORD=".env('MAIL_PASSWORD')."\nMAIL_ADDRESS=".env('MAIL_ADDRESS')."\nMAIL_NAME=".env('MAIL_NAME');
        $new = 'MAIL_HOST='.$data['smtp']."\nMAIL_PORT=".$data['port']."\nMAIL_USERNAME=".$data['emailaccount']."\nMAIL_PASSWORD=".$data['emailpwd']."\nMAIL_ADDRESS=".$data['sendaddress']."\nMAIL_NAME=".$data['sendname'];

        $update_str = str_replace($old, $new, $origin_str);

        file_put_contents(base_path('.env'), $update_str);
    }

}
