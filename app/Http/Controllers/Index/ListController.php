<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request,Session;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article,App\Tag,App\Comment,EndaEditor;
class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($list)
    {
        $articles = Article::NoDel()->where('cate_id',$list)->paginate(6);

        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.list',compact('cate','webset','articles','list'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($list,$id)
    {
        $article = Article::NoDel()->find($id);

        $article->content = EndaEditor::MarkDecode($article->content);
        
        $article->content = str_replace('<img ', '<img class="img-responsive img-thumbnail" ', $article->content);
        $article->content = str_replace('<a href=','<a target="_blank" href=',$article->content);
        $cate = $this->cate;

        $webset = $this->webset;

        $next = Article::NextArticle($list,$id)->first();
        
        $prev = Article::PrevArticle($list,$id)->first();

        $same = Article::SameArticle($list)->get();

        $comments = Comment::where(['art_id'=>$id,'parent_id'=>0,'check'=>1])->get();

        foreach($comments as $v){
            $v->content = EndaEditor::MarkDecode($v->content);
        }

        Article::find($id)->increment('click');


        return view('index.article',compact('cate','webset','article','next','prev','list','same','comments'));
    }
    
    /**
     * tag文章列表
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function tags($list)
    {
        $tag = Tag::find($list);

        $articles = $tag->articles()->where('del',0)->orderBy('published_at','desc')->paginate(6);
        
        $cate = $this->cate;

        $webset = $this->webset;
        
        return view('index.list',compact('cate','webset','articles','list','tag'));
        
    }
    /**
     * 搜索
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function search(Request $request)
    {
		if(!$request->contents){
			return redirect()->back();
		}
        $content = md5($request->contents);
        
        return redirect(url('search'.'/'.$request->contents.'/'.$content));
    }
	public function getSearch($search,$content)
	{
		$cate = $this->cate;
		$webset = $this->webset;
		if($content == md5($search)){
			$articles = Article::where('content','LIKE','%'.$search.'%')->where('del',0)->orderBy('published_at', 'desc')->paginate(6);
			return view('index.list',compact('articles','cate','webset','search'));
		}
		abort(404);

	}

}
