<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB,EndaEditor;

class PageController extends Controller
{
    /**
     * 时光轴
     * @return [type] [description]
     */
    public function getTimeline()
    {
        $cate = $this->cate;

        $webset = $this->webset;

        $timelines = DB::table('timeline')->orderBy('created_at','desc')->get();
        foreach($timelines as $v){
            $v->content = EndaEditor::MarkDecode($v->content);
        }
        
        return view('index.timeline',compact('cate','webset','timelines'));
    }
    /**
     * 关于我
     * @return [type] [description]
     */
    public function getAbout()
    {
        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.page',compact('cate','webset'));
    }

    public function addzan(Request $request)
    {
        $status = DB::table('timeline')->where('id',$request->id)->increment('zan');

        $zan = DB::table('timeline')->where('id',$request->id)->pluck('zan');

        return $zan;
    }
    public function getTree()
    {
        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.tree',compact('cate','webset'));
    }
    public function getSpider()
    {
        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.spider',compact('cate','webset'));
    }
    public function getShapes()
    {
        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.shapes',compact('cate','webset'));
    }
    public function getCloth(){
        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.cloth',compact('cate','webset'));
    }
}
