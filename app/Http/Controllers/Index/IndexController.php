<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article,App\Webset,App\Tag;

class IndexController extends Controller
{
    public function index()
    {
    	$webset = $this->webset;

    	$articles = Article::Recommend()->get();

    	$news = Article::NewArticle()->get();

		$tags = Tag::all();
		foreach($tags as $tag){
			$tag['color'] = self::tagColor($tag->id);
		}

    	$cate = $this->cate;
        
        return view('index.index',compact('webset','articles','news','cate','tags'));
    }

	public function tagColor($tag)
	{
		if($tag>6){
			$tag = $tag%2;
		}
		switch($tag){
			case 1:
				return 'default';
			case 2:
				return 'success';
			case 3:
				return 'primary';
			case 4:
				return 'danger';
			case 5:
				return 'info';
			case 6:
				return 'warning';
			default:
				return 'primary';
		}
	}
}
