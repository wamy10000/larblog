<?php

namespace App\Http\Controllers\Index;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User,Auth,Input;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct(){
        parent::__construct();
        $this->middleware('auth');
    }

    public function index()
    {
        $id = Auth::user()->id;

        $set =  User::find($id)->toArray();

        $cate = $this->cate;

        $webset = $this->webset;

        return view('home',compact('set','webset','cate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $id = Auth()->user()->id;
        
        $this->validate($request,
            ['name' =>  'required|min:2|max:255|unique:users,name,'.$id]
        );

        User::where('id',$id)->update($request->only('name','info'));

        return redirect(action('Index\HomeController@index'))->withSuccess('资料修改成功');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = User::find($id);

        $articles = $user->articles()->NoDel()->paginate(10);

        $cate = $this->cate;

        $webset = $this->webset;

        return view('index.usershow',compact('cate','webset','user','articles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

    }
    //头像上传处理
    public function avatar(Request $request){
        $id = Auth()->user()->id;
        //接收过来的是base64图像，检查是否有pic，是否可以去掉base64之前的字符
        if(!$request->has('pic') || !preg_match('/(?<=base64,)[\S|\s]+/',$request->pic,$match)){
            return redirect(action('Index\HomeController@index'))->withSuccess('头像上传失败');
        }else{
            $request->pic = $match[0];
            $picpath = public_path().'/avatar/'.$id.'.jpg';
            //是否可以把base64转为图片并保存
            if( !file_put_contents($picpath,base64_decode($match[0])) ){
                return redirect(action('Index\HomeController@index'))->withSuccess('头像上传失败,请检查目录权限');
            }
            $request->pic = 'avatar/'.$id.'.jpg';
            //保存路径信息
            User::where('id',$id)->update(['pic'=>$request->pic]);
            return redirect(action('Index\HomeController@index'))->withSuccess('头像修改成功'); 
        }        
    }

    public function pwded(){
        $email = auth()->user()->email;
        
        Auth::logout();
        return redirect(url('password/email'))->withMyemail($email);
    }
}
