<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,Input,App\User,Auth,Redirect;

class LoginController extends Controller
{
	public function loginPost(Request $request){
	    $validator = Validator::make(Input::all(),User::$rules);

	    $input = $request->except('_token');

	    if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']],isset($input['remember']) )) {
	        
	        if(Auth::user()->status == 0){
	        	Auth::logout();
	        	return redirect(url('/auth/login'))->withInput()->with('errorInfo','用户因违反规定已被禁用,无法登陆');
	        }
	        if(Auth::user()->type == 2){
	          return redirect('/home');
	        }elseif(Auth::user()->type == 0){
	        	return redirect('/yhsystem');
	        }
	    }
	    Auth::logout();
	    return redirect(url('/auth/login'))->withInput()->with('errorInfo','Email或密码错误')->withErrors($validator);

	}
}
