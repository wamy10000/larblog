<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Category,App\Webset;
abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
    	$cate = Category::where('p_id',0)->get();
        
        foreach($cate as $k=>$v){
            $cate[$k]['child'] = Category::where('p_id',$v['id'])->get();
        }
        //栏目信息调取
        $this->cate = $cate;

        $webset = Webset::find(1);
        //底部信息调取
        $this->webset = $webset;

    }

}
