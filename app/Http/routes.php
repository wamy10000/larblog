<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//首页
Route::get('/','Index\IndexController@index');
Route::get('errorer',function(){
	return view('html5');
});
//权限控制
Route::controllers([
    'auth'      =>  'Auth\AuthController',
    'password'  =>  'Auth\PasswordController',
]);

Route::group(['namespace'=>'Index'],function(){
	//前端用户相关
	Route::resource('home','HomeController',['only'=>['index','store','show']]);//个人中心
	Route::post('home/avatar','HomeController@avatar');//头像
	Route::get('reset/email','HomeController@pwded');//密码重置

	Route::get('lists{list}','ListController@index');//列表页
	Route::get('tags/{list}','ListController@tags');//tag文章
	Route::get('lists{list}/{id}','ListController@show');//文章
	Route::post('search','ListController@search');//搜索
	Route::get('search/{search}/{content}','ListController@getSearch');
	Route::controller('page','PageController');
	Route::post('addzan','PageController@addzan');//点赞

});

Route::group(['namespace'=>'Admin','middleware'=>'auth'],function(){
	//后台用户相关
	Route::get('yhsystem','AdminController@index');
	Route::get('yhsystem/index','AdminController@main');
	//用户设置
	Route::get('yhsystem/getUserInfo','AdminController@getUserInfo');
	Route::post('posts/getUserInfo','PostController@getUserInfo');
	//站点设置
	Route::get('yhsystem/webSet','AdminController@webSet');
	Route::post('posts/webSet','PostController@webSet');
	//邮箱配置
	Route::get('yhsystem/stemp','AdminController@stemp');
	Route::post('posts/stemp','PostController@stemp');
	//用户管理
	Route::get('yhsystem/myUser','AdminController@myUser');
	Route::post('posts/myUser','PostController@myUser');
	//角色管理
	Route::get('yhsystem/myRole','AdminController@myRole');
	Route::post('posts/myRole','PostController@myRole');
	Route::post('posts/delMyRole','PostController@delMyRole');
	Route::post('posts/register','PostController@register');
	//栏目管理	
	Route::resource('yhsystem/cate','CateController');
	Route::post('yhsystem/cate/add','CateController@add');//添加栏目
	Route::post('posts/sortCate','PostController@sortCate');//排序
	//文章管理
	Route::get('yhsystem/article/dust','ArticleController@dust');//回收站
	Route::get('yhsystem/article/runDust/{id}','ArticleController@runDust');
	Route::resource('yhsystem/article','ArticleController');
	Route::post('yhsystem/article/getCate','ArticleController@getCate');//获得指定栏目下的文章
	Route::post('yhsystem/article/setStatus','ArticleController@setStatus');//设置文章状态
	Route::post('yhsystem/article/upload','ArticleController@upload');//上传图片方法
	//说说
	Route::resource('yhsystem/talk','TalkController');

	//评论
	Route::resource('yhsystem/comment','CommentController',['only'=>['index','destroy','store']]);
    Route::post('setcomment','CommentController@setComment');
	//友情链接
	Route::resource('yhsystem/link','LinkController');
});

//登录处理相关
Route::post('login',[
		'as'			=>	'loginPost',
		'middleware'	=>	'guest',
		'uses'			=>	'LoginController@loginPost',
]);

