<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * 创建或修改文章时的表单认证
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         =>  'required|max:30',
            'content'       =>  'required',
            'click'         =>  'required|numeric',
            'status'        =>  'required|numeric',
            'published_at'  =>  'required|date'
        ];
    }
}
