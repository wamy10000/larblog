<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	protected $table = 'comment';

    protected $guarded = [];
	
    public $html;
	//关联文章
	public function article(){
		return $this->belongsTo('App\Article');
	}
	//关联作者
	public function user(){
		return $this->belongsTo('App\User');
	}
	//获取评论列表
    public static function getCommentData(){
    	$data = self::orderBy('created_at','desc')->paginate(20);
    	return $data;
    }
    //获取文章评论
    public static function getArtData($id){
    	$cate = self::where('art_id',$id)->orderBy('created_at','asc')->get();
    	$data = self::getSort($cate);
    	return $data;
    }
    //按回复排序
    public static function getSort($model,$pid=0,$level=0,$html=''){
    	$data = array();
    	foreach($model as $k=>$v){
    		if($v->parent_id == $pid){
    			if($level != 0){
    				$v->html = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;',$level);
    				$v->html .= '|';
    			}
    			$v->html .= str_repeat($html,$level);
    			$data[] = $v;
    			$data = array_merge($data,self::getSort($model,$v->id,$level+1));
    		}
    	}
    	return $data;
    }
    //获取所有评论的子评论
    public static function getChilds($m, $f_id) {
        $arr = array();
        foreach ($m as $v) {
            if ($v['parent_id'] == $f_id) {
                $arr[] = $v;
                $arr = array_merge($arr, self::getChilds($m, $v['id']));
            }
        }
        return $arr;
    }
}
