<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	protected $fillable = [
		'name'
	];
	//通过标签得到文章
    public function articles(){
    	return $this->belongsToMany('App\Article','article_tag','tag_id','art_id');
    }
}
