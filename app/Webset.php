<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webset extends Model
{
    protected $table = 'webset';

    protected $hidden = ['created_at', 'updated_at'];
    
}
