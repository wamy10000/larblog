<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article',function(Blueprint $table){
            $table->increments('id');
            $table->integer('cate_id');
            $table->integer('user_id')->unsigned();
            $table->string('title')->unique();
            $table->text('content');
            $table->string('keyword')->nullable();
            $table->string('description')->nullable();
            $table->integer('click');
            $table->integer('status')->default(0);//1.推荐2.置顶
            $table->integer('del')->default(0);
            $table->timestamp('published_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('article');
    }
}
