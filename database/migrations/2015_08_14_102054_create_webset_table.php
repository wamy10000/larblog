<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webset', function (Blueprint $table) {
            $table->increments('id');
            $table->string('webname');
            $table->string('website');
            $table->string('webinfo');
            $table->string('webemail');
            $table->string('webcode');
            $table->string('copyright');
            $table->string('keyword');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('webset');
    }
}
