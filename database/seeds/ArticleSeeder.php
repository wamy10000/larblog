<?php
use App\Article;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::insert([
        [
            'cate_id'		=>	10,
            'user_id'       =>  1,
            'title'			=>	'php教程',
            'content'		=>	'阿塑料袋卡斯蒂罗家啊师傅扩军备战 v 空间啊上课的酒吧看斯巴达克斯简单',
            'keyword'		=>	'PHP,Laravel,闫海',
            'description'	=>	'zheshimiaoshu,这是描述信息哦',
            'del'			=>	0,
            'published_at'  =>  date('y-m-d H:i:s',time())
        ],
        [
        	'cate_id'		=>	10,
            'user_id'       =>  1,
            'title'			=>	'11php教程2',
            'content'		=>	'11阿塑料袋卡斯蒂罗家啊师傅扩军备战 v 空间啊上课的酒吧看斯巴达克斯简单',
            'keyword'		=>	'PHP,Laravel,闫海',
            'description'	=>	'zheshimiaoshu,这是描述信息哦',
            'del'			=>	0,
            'published_at'  =>  date('y-m-d H:i:s',time())
        ],
        [
        	'cate_id'		=>	10,
            'user_id'       =>  1,
            'title'			=>	'22php教程',
            'content'		=>	'22阿塑料袋卡斯蒂罗家啊师傅扩军备战 v 空间啊上课的酒吧看斯巴达克斯简单',
            'keyword'		=>	'PHP,Laravel,闫海',
            'description'	=>	'zheshimiaoshu,这是描述信息哦',
            'del'			=>	0,
            'published_at'  =>  date('y-m-d H:i:s',time())
        ],
        [
        	'cate_id'		=>	9,
            'user_id'       =>  1,
            'title'			=>	'33php教程',
            'content'		=>	'33阿塑料袋卡斯蒂罗家啊师傅扩军备战 v 空间啊上课的酒吧看斯巴达克斯简单',
            'keyword'		=>	'PHP,Laravel,闫海',
            'description'	=>	'zheshimiaoshu,这是描述信息哦',
            'del'			=>	0,
            'published_at'  =>  date('y-m-d H:i:s',time())
        ],
        [
            'cate_id'		=>	9,
            'user_id'       =>  1,
            'title'			=>	'laravel教程',
            'content'		=>	'444阿塑料袋卡斯蒂罗家啊师傅扩军备战 v 空间啊上课的酒吧看斯巴达克斯简单',
            'keyword'		=>	'PHP,Laravel,闫海',
            'description'	=>	'zheshimiaoshu,这是描述信息哦',
            'del'			=>	0,
            'published_at'  =>  date('y-m-d H:i:s',time())
        ]

        ]);
    }
}
