<?php
use App\Tag;
use Illuminate\Database\Seeder;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::insert([
            [
                'name'  =>  'Laravel教程'
            ],
            [
                'name'  =>  'ThinkPHP教程'
            ],
            [
                'name'  =>  'Mac教程'
            ],
            [
                'name'  =>  'Git教程'
            ]
        ]);
    }
}
