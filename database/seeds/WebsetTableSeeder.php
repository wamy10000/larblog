<?php
use App\Webset;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class WebsetTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Webset::create([
        	'webname'	=>	'有聊',
        	'website'	=>	'www.yanhai0531.com',
        	'webinfo'	=>	'<a target="_blank" href="http://www.miitbeian.gov.cn/">鲁ICP备15004472号-1</a>',
        	'webemail'	=>	'34551471@qq.com',
        	'webcode'	=>	'',
        	'copyright'	=>	'Copyright © 2015 yanhai All Rights Reversed ',
        	'keyword'	=>	'有聊,闫海,芒果小叨',
        	'description'	=>	'无聊的时候不妨来有聊网转转，或许会有意想不到的收获。'
        	]);
    }
}
