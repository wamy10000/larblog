<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'  =>  'wamy10000',
            'email' =>  'wamy10000@126.com',
            'password'  =>  bcrypt('7986546'), 
            'type'  =>  0
        ]);
    }
}
