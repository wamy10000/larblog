<?php
use App\Category;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$config = [
    		[
        	'id'		=>	1,
        	'cate_name'	=>	'生活',
        	'p_id'		=>	0,
        	],
        	[
        	'id'		=>	2,
        	'cate_name'	=>	'PHP',
        	'p_id'		=>	0,
        	],
        	[
        	'id'		=>	3,
        	'cate_name'	=>	'WEB',
        	'p_id'		=>	0,
        	],
        	[
        	'id'		=>	4,
        	'cate_name'	=>	'大事件',
        	'p_id'		=>	0,
        	],
        	[
        	'id'		=>	6,
        	'cate_name'	=>	'日志',
        	'p_id'		=>	1,
        	],
        	[
        	'id'		=>	7,
        	'cate_name'	=>	'随笔',
        	'p_id'		=>	1,
        	],
        	[
        	'id'		=>	8,
        	'cate_name'	=>	'ThinkPHP',
        	'p_id'		=>	2,
        	],
        	[
        	'id'		=>	9,
        	'cate_name'	=>	'Laravel',
        	'p_id'		=>	2,
        	],
        	[
        	'id'		=>	10,
        	'cate_name'	=>	'基础',
        	'p_id'		=>	2
        	],
        	[
        	'id'		=>	11,
        	'cate_name'	=>	'进阶',
        	'p_id'		=>	2
        	],
        	[
        	'id'		=>	12,
        	'cate_name'	=>	'前端',
        	'p_id'		=>	3,	
        	],
        	[
        	'id'		=>	13,
        	'cate_name'	=>	'后端',
        	'p_id'		=>	3
        	]
        ];
        Category::insert($config);
    }
}
