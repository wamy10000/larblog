@extends('app')

@section('keywords')
{{$article->keyword}}
@stop

@section('description')
{{$article->description}}
@stop

@section('title')
{{$article->title}}
@stop

@section('cate')
@include('public.cate')
@stop
@section('beheader')
<link rel="stylesheet" href="http://cdn.bootcss.com/highlight.js/8.7/styles/railscasts.min.css">
@stop
@section('header')
<script src="http://cdn.bootcss.com/highlight.js/8.8.0/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
@stop
@section('content')
<script type="text/javascript" src="{{asset('public/js/comment.js')}}"></script>
<div class="container mt">
	<div class="row clearfix mt">
		<article class="mt animated slideInLeft">
		@include('errors.errorinfo')
			<div class="content bg-danger">
				<div class="projects-header page-header">
					<h2>{{$article->title}}</h2>
					<p>
						发布：<time title="{{$article->published_at}}" datetime="{{$article->published_at}}">{{$article->published_at}}</time>&nbsp;&nbsp;&nbsp;&nbsp;
                    作者：
                    <a href="{{url('home/'.$article->user->id)}}">{{$article->user->name}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    点击：{{$article->click}}
					</p>
				</div>
				<div class="pd" style="font-size: 18px">

					<p>{!!$article->content!!}</p>
				</div>
				<hr>
				<h3>标签：
					@forelse($article->tags as $v)
                    <a class="atag" href="{{url('tags/'.$v->id)}}"><span class="label label-info">{{$v->name}}</span></a>&nbsp;&nbsp;
                    @empty
                    无&nbsp;&nbsp;
                    @endforelse 
                    &nbsp;&nbsp;</h3>
                    <h3 class="text-success"><i class="glyphicon glyphicon-hand-down"></i> 分享这篇文章</h3>
                    <div class="bdsharebuttonbox"><a title="分享到QQ空间" href="#" class="bds_qzone" data-cmd="qzone"></a><a title="分享到新浪微博" href="#" class="bds_tsina" data-cmd="tsina"></a><a title="分享到微信" href="#" class="bds_weixin" data-cmd="weixin"></a><a title="分享到人人网" href="#" class="bds_renren" data-cmd="renren"></a><a title="分享到腾讯微博" href="#" class="bds_tqq" data-cmd="tqq"></a></div>
					<script>
						window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"1","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];
					</script>
			</div>
		</article>
		<nav>
			<ul class="pager">
			@if($prev)
				<li class="previous">
					<a href="{{url('lists'.$list.'/'.$prev->id)}}">
						<span aria-hidden="true">&larr;</span>
						{{$prev->title}}
					</a>
				</li>
			@else
				<li class="previous disabled">
					<a href="">
						<span aria-hidden="true">&larr;</span>
						无
					</a>
				</li>
			@endif
			@if($next)
				<li class="next">
					<a href="{{url('lists'.$list.'/'.$next->id)}}">
 {{$next->title}}						
<span aria-hidden="true">&rarr;</span>
						
					</a>
				</li>
			@else
				<li class="next disabled">
					<a href="">无
						<span aria-hidden="true">&rarr;</span>
						
					</a>
				</li>
			@endif
			</ul>
		</nav>
		<div class="well well-sm">
			<h4>相关文章</h4>
			<ul class="list-unstyled list-about">
			@foreach($same as $v)
				<li><a href="{{url('lists'.$list.'/'.$v->id)}}">{{$v->title}}</a></li>
			@endforeach
			</ul>
		</div>
	</div>
</div>
<div class="container content bg-warning">
	<h4>评论列表</h4>
	@foreach($comments as $comment)
	<div id="comment-{{$comment->id}}" class="comment">
		<a name="{{$comment->id}}"></a>
		<div class="avatar">
			<img width="40px" src="{{asset('avatar/'.$comment->user->id.'.jpg')}}"></div>
			<div class="comment-info">

				<b><a class="btn btn-sm" target="_blank" href="{{url('home/'.$comment->user->id)}}">{{$comment->user->name}} <span class="badge">{{$comment->id}}</span></a></b><br>	
				<span class="comment-time">{{$comment->created_at}}</span>
				<div class="comment-content">{!!$comment->content!!}</div>
				@if(Auth::user())
				<div class="comment-reply" style="padding-bottom: 5px;">
					<a class="btn btn-sm btn-success" onclick="commentReply({{$comment->id}},this)" href="#comment-{{$comment->id}}">回复</a>&nbsp;
					@if(Auth::user() && $comment->user->id == Auth::user()->id)
					<button status="{{$comment->id}}" type="button" class="btn btn-danger btn-sm btn-del" data-toggle="modal" data-target="#del">删除</button>
					@endif
				</div>
				@endif
			</div>
			@foreach(\App\Comment::getChilds($article->comments,$comment->id) as $v)
			<div id="comment-{{$v->id}}" class="comment comment-children">
				<a name="{{$v->id}}"></a>
				<div class="avatar"><img width="40px" src="{{asset('avatar/'.$v->user->id.'.jpg')}}"></div>
				<div class="comment-info"> <b><a class="btn btn-sm" target="_blank" href="{{url('home/'.$v->user->id)}}">{{$v->user->name}} <span class="badge">{{$v->id}}</span></a></b> 
					<br>	
					<span class="comment-time">{{$v->created_at}}</span>
					<div class="comment-content">回复<a class="btn btn-sm" href="{{url('home',\App\Comment::find($v->parent_id)->user->id)}}">{{\App\Comment::find($v->parent_id)->user->name}} <span class="badge">{{$v->parent_id}}</span></a>：{!! EndaEditor::MarkDecode($v->content) !!}</div>
					@if(Auth::user())
					<div class="comment-reply"> 
						<a class="btn btn-sm btn-success" onclick="commentReply({{$v->id}},this)" href="#comment-{{$v->id}}">回复</a>&nbsp;
						@if($v->user->id == Auth::user()->id)
						<button status="{{$v->id}}" type="button" class="btn btn-danger btn-sm btn-del" data-toggle="modal" data-target="#del">删除</button>
						@endif
					</div>
					@endif
				</div>
			</div>
			@endforeach
		</div>
	@endforeach
	<div id="comment-place">
		@if(Auth::user())
		<div id="comment-post" class="comment-post">
			<div style="display: none;padding-top:15px;" id="cancel-reply" class="cancel-reply"><a onclick="cancelReply()" href="javascript:void(0);">取消回复</a></div>
			<hr><p class="comment-header"><b>发表评论：</b><a name="respond"></a></p>
			<form id="commentform" action="{{url('yhsystem/comment/')}}" name="commentform" method="post">
				{!! csrf_field() !!}
				<input type="hidden" value="{{$article->id}}" name="art_id">
				<p><textarea tabindex="4"  rows="10" id="comment" name="contents" placeholder="支持Markdown语法"></textarea></p>
				<p class="text-danger"> <i class="glyphicon glyphicon-exclamation-sign"></i> 支持<b class="text-primary">markdown语法</b>，详情参考：<a href="https://laravist.com/discussion/21" target="_blank">查看</a></p>
				<p> <input type="submit" tabindex="6" value="发表评论" id="comment_submit" class="btn btn-success"></p>
				<input type="hidden" tabindex="1" size="22" value="0" id="comment-pid" name="parent_id">
			</form>
		</div>
		@else
		<div class="text-center" style="font-size:24px">请先<a href="{{url('auth/login')}}">登陆</a>或<a href="{{url('auth/register')}}">注册</a>再发表评论</div>
		@endif
	</div>
</div>
<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">确定删除这条评论？<br><i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;<span class='text-danger'>该评论的回复会被一起删除！！</span></div>
			<div class="modal-footer">
				<form action="" method="POST" class="form-del">
				
				<input type="hidden" name="_method" value="DELETE">
				{!! csrf_field() !!}
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				</form>

			</div>
		</div>
	</div>
</div>
@stop
@section('footer')
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$('#myTab a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});
//删除评论提交地址
$(".btn-del").click(function(){
	delId = $(this).attr('status');
	$('.form-del').attr('action',"{{url('yhsystem/comment')}}/"+delId);
});
$("#comment_submit").click(function(){
    var s = $('#comment').val();
	if(!s){
	    alert('请先输入评论内容再提交！');
	    return false;
	}
    if(!/.*[\u4e00-\u9fa5]+.*$/.test(s)){
        alert('评论必须包含中文!');
        return false;
    }
    if(s.length > 300){
        alert('评论不能超过300个字符!');
        return false;
    }
});
(function(){
    var bp = document.createElement('script');
    bp.src = '//push.zhanzhang.baidu.com/push.js';
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script>
@stop
