@extends('app')

@section('keyword')
{{$webset->keyword}}
@stop

@section('description')
{{$webset->description}}
@stop

@section('title')
有聊
@stop

@section('indexwell')
@include('public.indexwell')
@stop

@section('cate')
@include('public.cate')
@stop

@section('indexinfo')
@include('public.indexinfo')
@stop

@section('content')
<div class="container">
	<div class="row clearfix">
		<div class="col-md-9 column">
			<div class="page-header"  id="content">
				<h1  class="animated zoomInRight">
					推荐文章
					<small>或许会有所收获？</small>
				</h1>
			</div>
			<div class="row clearfix">
				<div class="col-md-12 column">
				@foreach($articles as $article)
					<div class="left-art">
						<h2><a target="_blank" href="{{url('lists'.$article->cate_id.'/'.$article->id)}}">{{$article->title}}</a></h2>
						<p>
							{{$article->description}}<a class="btn" href="{{url('lists'.$article->cate_id.'/'.$article->id)}}" target="_blank">查看更多 »</a>
						</p>
						<p>
							<i class="glyphicon glyphicon-user"></i>作者：<a href="{{url('home/'.$article->user->id)}}">{{$article->user->name}}</a>&nbsp;&nbsp;
							<i class="glyphicon glyphicon-tag"></i>标签：
								@forelse($article->tags as $v)
								<a class="atag" href="{{url('tags/'.$v->id)}}"><span class="label label-info">{{$v->name}}</span></a>&nbsp;&nbsp;
								@empty
								无&nbsp;&nbsp;
								@endforelse						
							<i class="glyphicon glyphicon-time"></i><time class="post-date" title="{{$article->published_at}}" datetime="{{$article->published_at}}">{{$article->published_at}}</time>
						</p>
					</div>
				@endforeach	
				</div>
			</div>
		</div>
		<div class="col-md-3 column">
			<div class="thumbnail text-center">
				<h3>芒果小叨</h3>
				<img class="img-circle" alt="140x140" src="{{asset('public/images/default.jpg')}}" />
				<div class="caption">
					<p>什么都略懂，会点儿三脚猫功夫。</p>
					<a class="btn btn-danger btn-large" href="{{url('page/about')}}">About Me</a>
				</div>
			</div>
			<div class="right-new">
				<h4>最新发布</h4>
				<ul class="list-unstyled">
				@foreach($news as $new)
					<li>
						<a target="_blank" href="{{url('lists'.$new->cate_id.'/'.$new->id)}}">{{$new->title}}</a>
					</li>
				@endforeach
				</ul>
			</div>
			<div class="right-tag mt">
                <h4>云标签</h4>
                <ul class="list-unstyled clearfix">
                @foreach($tags as $tag)
                <a href="{{url('tags/'.$tag->id)}}"><span class="label label-{{$tag->color}}">{{$tag->name}}</span></a>
                @endforeach
                </ul>
            </div>
		</div>
	</div>
</div>
<script type="text/javascript">
(function(){
    var bp = document.createElement('script');
    bp.src = '//push.zhanzhang.baidu.com/push.js';
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script>
@stop