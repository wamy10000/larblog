@extends('app')

@section('keyword')
时光轴，芒果小叨
@stop

@section('description')
有聊网的时光轴
@stop

@section('title')
时光轴
@stop

@section('cate')
@include('public.cate')
@stop

@section('content')

<div class="container mt">
<div class="jumbotron clearfix" style="background-image: url(http://image.golaravel.com/5/c9/44e1c4e50d55159c65da6a41bc07e.jpg);box-shadow: 10px 10px 5px #888888;">
  <h1>时光轴</h1>
  <p class="animated shake">回不去的逝水流年。</p>
  <p><a class="anchorjs-link" href="#cd-timeline">
<button class="btn btn-info btn-lg animated flash pull-right" onclick="$('#cd-timeline').animatescroll({scrollSpeed:2000,easing:'easeOutBounce'});">查看历史</button>
</a></p>
</div>

    <section id="cd-timeline" class="cd-container">
        @foreach($timelines as $timeline)
        <div class="cd-timeline-block">
            <div class="cd-timeline-img 
            @if($timeline->id %2 == 0)
            cd-movie
            @else
            cd-picture
            @endif
            cd-picture">                
            </div><!-- cd-timeline-img -->
            <div class="cd-timeline-content" style="box-shadow: 10px 10px 5px #888888;">                
                <p class="text-primary"><em class="text-warning">芒果小叨:</em> {!!$timeline->content!!}</p>
                <div class="text-danger"><span class="zan-{{$timeline->id}}">赞：{{$timeline->zan}}</span> <button status="{{$timeline->id}}" class="btn btn-sm btn-warning btn-zan pull-right"><i class="glyphicon glyphicon-thumbs-up"></i></button></div>
                
                
                <span class="cd-date">{{$timeline->created_at}}</span>
            </div> <!-- cd-timeline-content -->
        </div> <!-- cd-timeline-block -->
        @endforeach
    </section> <!-- cd-timeline -->
<script type="text/javascript" src="{{asset('public/js/timeline.js')}}"></script>    
<script>
$(function(){
    var $timeline_block = $('.cd-timeline-block');
    //hide timeline blocks which are outside the viewport
    $timeline_block.each(function(){
        if($(this).offset().top > $(window).scrollTop()+$(window).height()*0.75) {
            $(this).find('.cd-timeline-img, .cd-timeline-content').addClass('is-hidden');
        }
    });
    //on scolling, show/animate timeline blocks when enter the viewport
    $(window).on('scroll', function(){
        $timeline_block.each(function(){
            if( $(this).offset().top <= $(window).scrollTop()+$(window).height()*0.75 && $(this).find('.cd-timeline-img').hasClass('is-hidden') ) {
                $(this).find('.cd-timeline-img, .cd-timeline-content').removeClass('is-hidden').addClass('bounce-in');
            }
        });
    });
    $('html').attr('style','font-size:18px');

    $('.btn-zan').click(function(){
        var id = $(this).attr('status');
        $.post("{{url('addzan')}}",{'_token': '{{ csrf_token() }}','id':id},function(sta){
            $('.zan-'+id).html('赞：'+sta);
        },'json');
    });

    $().delay()
});
</script>
</div>
@stop
