@extends('app')

@section('keyword')
{{$user->name}},有聊网会员
@stop

@section('description')
有聊网。
@stop

@section('title')
高级会员:{{$user->name}}
@stop

@section('cate')
@include('public.cate')
@stop

@section('content')
<div class="container mt100">
    <div class="row clearfix" style="background-image: url({{asset('public/images/banner.jpg')}});border-radius: 5px">
        <div class="col-md-12 column mt text-center">
            <a class="opacity1" href="">
            <img class="center-block img-circle" src="{{asset('avatar/'.$user->id.'.jpg')}}">
                <h1 style="color:#fff;">{{$user->name}}</h1>
            </a><br>
            <h2 class="bg-success text-danger pd">{{$user->info}}</h2>
        </div>
    </div>

    <div class="col-md-12 column">
            <div class="page-header">
                <h1>
                    该会员发布的文章
                </h1>
            </div>
        <div class="col-md-12 column">
        @forelse($articles as $article)
            <div class="jumbotron well">
                <h2 style="border-bottom: 1px solid #e8e5e5;padding-bottom: 10px;">
                    {{$article->title}}
                </h2>
                <p>
                    {{$article->description}}
                </p>
                <div class="pull-right"><a href="{{url('lists'.$article->cate_id.'/'.$article->id)}}" class="btn btn-primary">阅读全文</a></div>
                <p> 
                    <i class="glyphicon glyphicon-user"></i>
                    作者：
                    <a href="/author/johnlui/">{{$article->user->name}}</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-tag"></i>
                    标签：
                    @forelse($article->tags as $v)
                    <a class="atag" href="{{url('tags/'.$v->id)}}"><span class="label label-info">{{$v->name}}</span></a>&nbsp;&nbsp;
                    @empty
                    无&nbsp;&nbsp;
                    @endforelse 
                    &nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i>发布时间：
                    <time class="post-date" title="{{$article->published_at}}" datetime="{{$article->published_at}}">{{ explode(' ',$article->published_at)[0] }}</time>
                </p>
            </div>
        @empty
        暂时没有相关内容.
        @endforelse
        </div>
        <nav class="text-center">
            {!! $articles->render() !!}
        </nav>

    </div>
</div>

@stop