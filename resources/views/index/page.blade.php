@extends('app')

@section('title')
关于我
@stop

@section('cate')
@include('public.cate')
@stop

@section('content')
<div class="container mt50">

    <!-- Docs nav
    ================================================== -->
    <div class="row mt">
        <div class="col-md-9 animated zoomInDown">
            <article class="content">
                <section class="pd text-warning clearfix">
                    
<h2 class="text-danger">Just about me</h2>

    <p>平时事情还是比较多的，粗略算了算我至少已经2个月没和朋友们打交道了。</p>

    <p>上班，下班，吃饭，睡觉。我一如既往的生活在自己的小圈子里不问世事，喜怒哀乐与自己分享。我开开心心的学技术，进步并快乐着，我出了自己的视频教程帮助需要帮助的人，我开发了自己的个人网站，一切都继续着。。。</p>

    <p>至于为什么要这样做，上午和波总聊天的时候又提到了，别荒废自己的青春。</p>

    <p>我不是从事这个工作的，也没人逼我去学技术，那为什么要坚持呢？因为路都是自己选的，做自己喜欢的事情永远是快乐的。</p>

    <p>我孤独的前行着，没有任何人教，完全自学。相比一些号称参加过万元魔鬼式培训出来的程序员，就目前来看我并不比他们差。</p>

    <p>一年前我说过我要坚持下来，一年后我自豪的说我做到了！</p>

    <p>我一直说《阿甘正传》是我最喜欢的一部电影，因为他一直激励着我前进。。。</p>
    <blockquote>
    <p>sometimes， you do something but not knowing its meaning</p>

    <p>just run</p>

    <p>run for what</p>

    <p>for dream ，for life，for jenny ，for something others</p>

    <p>if you do not run， you will never know what a person you are</p>

    <p>when we are tired， heading home</p>

    <p>when we are hungry， researching something to eat.</p>

    <p>A simple person leads a simple life.</p>

    <p>Why should we always pretend to be clever?</p>

    <p>Do yourself.</p>

    <p>a real man..</p>
    </blockquote>

    

    <p class="text-info">在此感谢过去一年在学习和技术上给与过我帮助人：@撰天 @老灰 @孤独的行者 @Mr.Apple @沉寂丶 @阿飞 
    感谢我的家人和朋友们，太多不一一@了</p>
    <p class="text-danger">最后感谢@草莓小兔 在我困难的时候陪伴我共进退！</p>
<hr>
    <h3 class="text-primary text-center">About my blog</h3>




            <dl class="dl-horizontal">
                <dt>
                    域名：
                </dt>
                <dd>
                    www.yanhai0531.com
                </dd>
                <dt>
                    服务器：
                </dt>
                <dd>
                    阿里云服务器
                </dd>
                <dt>
                    备案号：
                </dt>
                <dd>
                    鲁ICP备15004472号-1
                </dd>
                <dt>
                    程序：
                </dt>
                <dd>
                    本网站由Laravel5.1框架强力驱动
                </dd>
            </dl>

                </section>
            </article>
        </div>
        <div class="col-md-3 animated rollIn">
            <div class="thumbnail text-center">
                <h3>芒果小叨</h3>
                <img class="img-circle" alt="140x140" src="{{asset('public/images/default.jpg')}}" />
                <div class="caption">
                    <p>什么都略懂，会点儿三脚猫功夫。</p>
                </div>
            </div>
            <ul class="list-group-item">
                <li class="list-group-item">
                    <a target="_blank" title="PHP小圈子" href="http://shang.qq.com/wpa/qunwpa?idkey=80e2658f696fd5cc2e7a8359137a5c202065200a574eabb546c18646cb32c4bb"> <i class="fa fa-qq"></i>
                        PHP自学QQ群
                    </a>
                </li>
                <li class="list-group-item"><a style="text-decoration:none;" href="http://mail.qq.com/cgi-bin/qm_share?t=qm_mailme&email=NgUCAwMHAgEHdkdHGFVZWw" target="_blank"> <i class="fa fa-comments"></i>
                    邮箱：
                    <span id="qqgroup">34551471@qq.com</span></a>
                </li>        
                <li class="list-group-item">
                    <a target="_blank" title="芒果小叨的微博" href="http://weibo.com/u/1110073224">
                        <i class="fa fa-weibo"></i>
                        新浪微博：@芒果小叨
                    </a>
                </li>
            </ul>
        </div>
    </div>

</div>
<script type="text/javascript">
(function(){
    var bp = document.createElement('script');
    bp.src = '//push.zhanzhang.baidu.com/push.js';
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script>
@stop