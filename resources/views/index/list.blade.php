@extends('app')

@if(isset($list))
@section('keywords')
芒果小叨,闫海,有聊网,有聊,{{\App\Category::where('id',$list)->pluck('cate_name')}}
@stop
@endif

@section('description')
@if(isset($tag))
    {{$tag->name}}
@elseif(isset($list))
    {{\App\Category::where('id',$list)->pluck('cate_name')}}
@elseif($search)
    {{$search}} 的搜索结果
@else
    搜索结果
@endif
@stop

@section('title')
@if(isset($tag))
    {{$tag->name}}
@elseif(isset($list))
    {{\App\Category::where('id',$list)->pluck('cate_name')}}
@elseif($search)
    {{$search}} 的搜索结果
@else
    搜索结果
@endif
@stop

@section('cate')
@include('public.cate')
@stop

@section('content')
<div class="container mt50">
    <div class="row clearfix">
        <div class="page-header">
            <h1 class="animated rollIn">
            @if(isset($tag))
                {{$tag->name}}
            @elseif(isset($list))
                当前位置：
                <small>{{\App\Category::where('id',$list)->pluck('cate_name')}}</small>
            @elseif($search)
                {{$search}} 的搜索结果
            @else
                搜索结果
            @endif
            </h1>
        </div>
        <div class="col-md-12 column animated fadeInLeft">
        @forelse($articles as $article)
            <div class="jumbotron well">
                <h2 style="border-bottom: 1px solid #e8e5e5;padding-bottom: 10px;">
                    @if($article->status == 2)
                    <span class="text-primary">[顶]{{$article->title}}</span>
                    @elseif($article->status == 1)
                    <span class="text-info">[荐]{{$article->title}}</span>
                    @else
                    {{$article->title}}
                    @endif
                </h2>
                <p>
                    {{$article->description}}<a class="pull-right btn btn-primary" target="_blank" href="{{url('lists'.$article->cate_id.'/'.$article->id)}}">阅读全文</a>
                </p>
                <p> 
                    <i class="glyphicon glyphicon-user"></i>
                    作者：
                    <a href="{{url('home/'.$article->user->id)}}">{{$article->user->name}}</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="glyphicon glyphicon-tag"></i>
                    标签：
                    @forelse($article->tags as $v)
                    <a class="atag" href="{{url('tags/'.$v->id)}}"><span class="label label-info">{{$v->name}}</span></a>&nbsp;&nbsp;
                    @empty
                    无&nbsp;&nbsp;
                    @endforelse 
                    &nbsp;&nbsp;<i class="glyphicon glyphicon-time"></i>发布时间：
                    <time class="post-date" title="{{$article->published_at}}" datetime="{{$article->published_at}}">{{ explode(' ',$article->published_at)[0] }}</time>
                </p>
            </div>
        @empty
        暂时没有相关内容.
        @endforelse
        </div>
        <nav class="text-center">
            {!! $articles->render() !!}
        </nav>
    </div>
</div>
<script type="text/javascript">
(function(){
    var bp = document.createElement('script');
    bp.src = '//push.zhanzhang.baidu.com/push.js';
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(bp, s);
})();
</script>
@stop