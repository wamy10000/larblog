<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$('#myTab a').click(function (e) {
e.preventDefault();
$(this).tab('show');
});
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		@include('errors.errorinfo')
			<div style="padding: 10px;" class="col-sm-8">
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#home">网站信息</a>
					</li>

				</ul>
				<div class="tab-content" id="myTabContent" style="padding:10px">
					<div aria-labelledby="home-tab" id="home" class="tab-pane fade active in" role="tabpanel">
						<form class="form-horizontal" method="POST" action="{{url('posts/webSet')}}">
							<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<div class="form-group">
								<label for="webname" class="col-sm-4 control-label">网站名称</label>
								<div class="col-sm-8">
									<input type="webname" class="form-control" id="webname" name="webname" value="{{$webname}}"></div>
							</div>
							<div class="form-group">
								<label for="website" class="col-sm-4 control-label">网站域名</label>
								<div class="col-sm-8">
									<input type="website" class="form-control" id="website" name="website" value="{{$website}}"></div>
							</div>
							<div class="form-group">
								<label for="keyword" class="col-sm-4 control-label">网站关键字</label>
								<div class="col-sm-8">
									<input type="keyword" class="form-control" id="keyword" name="keyword" value="{{$keyword}}"></div>
							</div>
							<div class="form-group">
								<label for="description" class="col-sm-4 control-label">网站描述</label>
								<div class="col-sm-8">
									<textarea type="description" class="form-control" rows="5" id="mywords" name="description">{{$description}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="webinfo" class="col-sm-4 control-label">备案信息</label>
								<div class="col-sm-8">
									<input type="webinfo" class="form-control" id="webinfo" name="webinfo" value="{{$webinfo}}"></div>
							</div>
							<div class="form-group">
								<label for="webemail" class="col-sm-4 control-label">站长邮箱</label>
								<div class="col-sm-8">
									<input type="webemail" class="form-control" id="webemail" name="webemail" value="{{$webemail}}"></div>
							</div>
							<div class="form-group">
								<label for="webcode" class="col-sm-4 control-label">统计代码</label>
								<div class="col-sm-8">
									<textarea class="form-control" rows="5" id="webcode" name="webcode">{{$webcode}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="copyright" class="col-sm-4 control-label">版权信息</label>
								<div class="col-sm-8">
									<textarea class="form-control" rows="5" id="copyright" name="copyright">{{$copyright}}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-11">
									<input type="submit" class="btn btn-primary" value="修改"></div>
							</div>
						</form>
					</div>					
				</div>
			</div>
			
		</div>
	</div>

</body>
</html>