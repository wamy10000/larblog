<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>asd</title>
	<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
	<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		@include('errors.errorinfo')
			<div style="padding: 10px;" class="col-sm-6">
				<ul class="nav nav-tabs">
					<li class="active" role="presentation">
						<a href="" data-toggle="tab">SMTP配置</a>
					</li>
				</ul>
				<div class="tab-content" style="padding:10px">
					<form class="form-horizontal" method="POST" action="{{url('posts/stemp')}}">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
						<div class="form-group">
							<label for="sendname" class="col-sm-4 control-label">发件人</label>
							<div class="col-sm-8">
								<input type="sendname" name="sendname" class="form-control" id="sendname" value="{{ env('MAIL_NAME') }}"></div>
						</div>
						<div class="form-group">
							<label for="sendaddress" class="col-sm-4 control-label">邮箱地址</label>
							<div class="col-sm-8">
								<input type="sendaddress" class="form-control" id="sendaddress" name="sendaddress" value="{{ env('MAIL_ADDRESS') }}"></div>
						</div>
						<div class="form-group">
							<label for="smtp" class="col-sm-4 control-label">SMTP服务器</label>
							<div class="col-sm-8">
								<input type="smtp" class="form-control" id="smtp" name="smtp" value="{{ env('MAIL_HOST') }}"></div>
						</div>
						<div class="form-group">
							<label for="port" class="col-sm-4 control-label">服务器端口号</label>
							<div class="col-sm-8">
								<input type="port" class="form-control" id="port" name="port" value="{{ env('MAIL_PORT') }}"></div>
						</div>
						<div class="form-group">
							<label for="emailaccount" class="col-sm-4 control-label">邮箱登陆账号</label>
							<div class="col-sm-8">
								<input type="emailaccount" class="form-control" id="emailaccount" name="emailaccount" value="{{ env('MAIL_USERNAME') }}"></div>
						</div>
						<div class="form-group">
							<label for="emailpwd" class="col-sm-4 control-label">邮箱登陆密码</label>
							<div class="col-sm-8">
								<input type="emailpwd" class="form-control" id="emailpwd" name="emailpwd" value="{{ env('MAIL_PASSWORD') }}"></div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-11">
								<input type="submit" class="btn btn-info btn-lg" value="修改"></div>
						</div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</body>
</html>