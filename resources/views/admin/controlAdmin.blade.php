<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$('#myTab a').click(function (e) {
e.preventDefault();
$(this).tab('show');
});
$(function(){
	$('[data-toggle=tooltip]').tooltip();
	$('.tbtn').click(function(){
		$status = $(this).attr('mystatus');
		$name = $(this).parent().siblings('td').eq(2).text();
		$info = $(this).parent().prev('td').text();
		$('input[name="id"]').attr('value',$status);
		$('input[name="name"]').attr('value',$name);
		$('textarea[name="info"]').text($info);
	})	
});
</script>
</head>
<body>
	<div class="container-fluid">
	@include('errors.errorinfo')
		<div class="row">
			<div style="padding: 10px;" class="col-sm-12">
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="control" data-toggle="tab" role="tab" id="control-tab" href="#control">管理员</a>
					</li>
					<li class="pull-right">
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#add">添加管理员</button>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent" style="padding:10px">
					<div aria-labelledby="control-tab" id="control" class="tab-pane fade active in" role="tabpanel">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>邮箱地址</th>
									<th>昵称</th>
									<th>头像</th>
									<th>最后登录时间</th>
									<th>个性签名</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($myusers as $user )
								<tr>
									<td>{{$user['id']}}</td>
									<td>{{$user['email']}}</td>
									<td>{{$user['name']}}</td>
									<td>
									<a data-placement="right" data-toggle="tooltip" href="#" data-html='1' data-original-title="<img src='{{asset($user["pic"])}}' width='100' />"><img src='{{asset($user["pic"])}}' width="20" /></a>
									</td>
									<td>{{$user['updated_at']}}</td>
									<td>{{$user['info']}}</td>
									<td>
									@if( Auth::user()->id == 1 || Auth::user()->id == $user['id'] )
									<button mystatus="{{$user['id']}}" type="button" class="btn btn-danger btn-sm tbtn" data-toggle="modal" data-target="#edit">修改</button> 
									@endif
									@if( Auth::user()->id == 1 && Auth::user()->id != $user['id'] )
									<button mystatus="{{$user['id']}}" type="button" class="btn btn-success btn-sm tbtn" data-toggle="modal" data-target="#del">删除</button>
									@endif
									@if( Auth::user()->id != 1 && Auth::user()->id != $user['id'] )
										普通管理员无权操作
									@endif
									</td>
								</tr>
								@endforeach
								
							</tbody>
						</table>
					</div>
				</div>
				<div class="pull-right" style="padding-right: 100px">
					<nav>
					{!! $myusers->render() !!}
					</nav>
				</div>
			</div>
		</div>
	</div>
<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">提示信息</h4>
			</div>
			<div class="modal-body">确定删除该管理员？</div>
			<div class="modal-footer">
			<form action="{{url('posts/delMyRole')}}" method="POST">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="id" value="">
				<button type="submit" class="btn btn-success">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</form>	
			</div>
		</div>
	</div>
</div>

<!-- 修噶管理员 -->
<div id="edit" class="modal fade bs-example-modal-lg row" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">修改管理员</h4>
			</div>
			<form class="form-horizontal" method="POST" action="{{url('posts/myRole') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" value="">
			<div class="modal-body">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">昵称</label>
					<div class="col-sm-8">
						<input class="form-control" name="name" id="name" type="text" value="" />
					</div>
				</div>
				<div class="form-group">
					<label for="passwd" class="col-sm-2 control-label">个性签名</label>
					<div class="col-sm-8">
						<textarea class="form-control" name="info" rows="3" id="info"></textarea>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-6">
						<input type="submit" class="btn btn-info btn-lg" value="修改"><button type="button" class="btn btn-default btn-lg" data-dismiss="modal">取消</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<!-- 添加管理员 -->
<div id="add" class="modal fade bs-example-modal-lg row" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加管理员</h4>
			</div>
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/posts/register') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="modal-body" style="margin:10px">
				<div class="form-group">
					 <label for="exampleInputEmail1">邮箱:</label><input class="form-control" id="exampleInputEmail1" type="email" name="email" placeholder="请输入邮箱地址"/>
				</div>
				<div class="form-group">
					 <label for="examplename">昵称:</label><input class="form-control" id="examplename" type="name" name="name" placeholder="请输入昵称"/>
				</div>
				<div class="form-group">
					 <label for="exampleInputPassword1">密码:</label><input class="form-control" id="exampleInputPassword1" type="password" name="password" placeholder="请输入密码"/>
				</div>
				<div class="form-group">
					 <label for="exampleInputPassword2">确认密码:</label><input class="form-control" id="exampleInputPassword2" type="password" name="password_confirmation" placeholder="请输入密码"/>
				</div>
			</div>
			<div class="modal-footer">
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-6">
						<input type="submit" class="btn btn-success btn-lg" value="添加"><button type="button" class="btn btn-default btn-lg" data-dismiss="modal">取消</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>