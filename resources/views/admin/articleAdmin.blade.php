<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link href="{{ asset('public/admin/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />

<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>

<script type="text/javascript">

$(function(){
	$('[data-toggle=tooltip]').tooltip();
	$('.btn-update').click(function(){
		var sta = $(this).parent('div').children('input');
		sta.attr('value',$(this).attr('value'));
	});
	$('.btn-delete').click(function(){
		var del = $(this).parent('td').siblings('td').first().children('input').attr('value');
		var delid = "{{url('yhsystem/article/runDust')}}/" + del;
		$('.btn-del').attr('href',delid);
	});
	//全选or全不选
	$("#checkAll").bind({
		click:function(){
			if(this.checked){
		        $('[type=checkbox]').prop('checked',true);
		    }else{
		        $('[type=checkbox]').prop('checked',false);
		    }
		}
	});
});
</script>
<style type="text/css">
	.pd{padding:10px;}
	.mg{margin:10px;}
	.input50{width: 50px;}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12 pd">
				@include('errors.errorinfo')
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="control" data-toggle="tab" role="tab" id="control-tab" href="#control">文章管理</a>
					</li>

						<a class="btn btn-warning pull-right" href="{{url('yhsystem/article/create')}}">发布文章</a>

				</ul>
				<div class="tab-content pd" id="myTabContent">
					<div aria-labelledby="control-tab" id="control" class="tab-pane fade active in" role="tabpanel">
						<!--顶部form-->
						<form action="{{URL('yhsystem/article/getCate')}}" method="post" class="bg-info form-inline pd">
						{!! csrf_field() !!}
							<div class="form-group">
								<label for="classify">分类：</label>
								<select name="cate_id" class="form-control" id='classify'>
									<option value="0">全部</option>
									@foreach($cates as $cate)
									<option value="{{$cate['id']}}">{{ $cate['html'].$cate["cate_name"]}}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group" style="padding-left:10px">
								<label for="keywords">关键字</label>
								<input type="text" placeholder="请输入关键字..." id="keywords" style="width:200px;" name="keywords" class="form-control">
								<input type="hidden" name='del' value=0>
							</div>
							<button type="submit" class="btn btn-default">搜索</button>
						</form>
						<!--顶部form结束-->
						<!--body form-->
						<form action="{{url('yhsystem/article/setStatus')}}" method="POST">
							{!! csrf_field() !!}
							<table class="table table-hover">
								<thead>
									<tr>
										<th></th>
										<th>ID</th>
										<th>标题</th>
										<th>点击量</th>
										<th>摘要</th>
										<th>发布人</th>
										<th>发布时间</th>
										<th>状态</th>
										<th>操作</th>
									</tr>
								</thead>
								<tbody>
									@forelse($articles as $article)
									<tr>
										<td><input name="ids[]" type="checkbox" value="{{$article->id}}"></td>
										<td>{{$article->id}}</td>
										<td>{{str_limit($article->title,20)}}</td>
										<td>{{$article->click}}</td>
										<td>{{str_limit($article->description,15)}}</td>
										<td>{{$article->user->name}}</td>
										<td>{{$article->published_at}}</td>
										<td>
										@if($article->status == 2)
										<i class="glyphicon glyphicon-ok"></i>置顶
										@elseif($article->status == 1)
										<i class="glyphicon glyphicon-ok"></i>推荐
										@endif
										</td>
										<td><a class="btn btn-info btn-sm" href="{{url('yhsystem/article/'.$article->id.'/edit')}}">修改</a> <button type="button" class="btn btn-danger btn-sm btn-delete" data-toggle="modal" data-target="#mydel">删除</button></td>
									</tr>
									@empty
									@if(isset($info))
									<tr><td colspan="9">在栏目 <b>{{$info['cate_name']}}</b> 下没有找到有关 <b>{{$info['keywords']}}</b> 的相关内容。</td></tr>
									@endif
									@endforelse

								</tbody>
							</table>
							<div class="bg-info form-inline pd">
								<input type="checkbox" id="checkAll">&nbsp;全选&nbsp;&nbsp;
								<input type="hidden" name="status" value="">
								<button class="btn btn-sm btn-success btn-update" value="2">置顶</button>
								<button class="btn btn-sm btn-danger btn-update" value="1">推荐</button>
								<button class="btn btn-sm btn-warning btn-update" value="0">普通文章</button>
							</div>
						</form>
						<!--body form结束-->
						<!--分页-->
						<div class="pull-right" style="padding-right: 100px">
							<nav>
								{!! $articles->render() !!}
							</nav>
						</div>
						<!--分页结束-->
					</div>

				</div>

			</div>
		</div>
	</div>
<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="mydel" tabindex="-11" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">提示信息</h4>
			</div>
			<div class="modal-body">确定移入回收站？</div>
			<div class="modal-footer">
				<a href="" class="btn btn-danger btn-del">确认</a>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</div>
		</div>
	</div>
</div>

</body>
<script type="text/javascript" src="{{ asset('public/admin/js/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/lang/bootstrap-datetimepicker.zh-CN.js') }}" charset="UTF-8"></script>
<script type="text/javascript">
	$('.form_date').datetimepicker({
        language:  'zh-CN',
    });
    $('#tags').select2({
    	tags:true,
    	placeholder:'请选择文章标签'
    });
</script>
</html>


