<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Document</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
</head>
<body>
<div class="container-fluid">
	<div style="padding-top:5px;">
		<div class="alert alert-success">
			<p><script type="text/javascript">maindata();</script></p>
		</div>
	</div>

	<div class="row">		
		<div class="col-md-6">
			<div class="col-sm-12">
			<div class="panel panel-info">
				<div class="panel-heading">
				    <h3 class="panel-title">个人信息</h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover">
						<tbody>
							<tr>
								<td colspan="2" class="text-info">您好，{{Auth::user()->name}}.</td>
							</tr>
							<tr>
								<td colspan="2" class="text-success">{{Auth::user()->info}}</td>
							</tr>

							

						</tbody>
					</table>
				</div>
			</div>
			</div>
			<div class="col-sm-12">
			<div class="panel panel-warning">
				<div class="panel-heading">
				    <h3 class="panel-title">服务器信息</h3>
				</div>
				<div class="panel-body">
					<table class="table table-hover">
						<tbody>
							<tr>
								<td>操作系统</td>
								<td>{{php_uname('s')}}</td>
							</tr>
							<tr>
								<td>运行环境</td>
								<td>{{ php_sapi_name()}}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</div>	
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
					    <div class="col-xs-9">
					        <div class="huge">今日热点新闻</div>
					    </div>
					</div>
				</div>
				<div class="panel-footer">
				<script language="JavaScript" type="text/JavaScript" src="http://news.baidu.com/n?cmd=1&class=sportnews&pn=1&tn=newsbrofcu"></script></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="row">
					    <div class="col-xs-9">
					        <div class="huge">说说信息</div>
					    </div>
					</div>
				</div>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">说说总量::{{$allTalk}}</span>
						<span class="pull-right"><i class="glyphicon glyphicon-volume-up"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">今日发布::{{$toTalk}}</span>
						<span class="pull-right"><i class="glyphicon glyphicon-volume-up"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>

		<div class="col-md-4">
			<div class="panel panel-danger">
				<div class="panel-heading">
					<div class="row">
					    <div class="col-xs-9">
					        <div class="huge">文章数量</div>
					    </div>
					</div>
				</div>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">发布总量：{{$allArt}}</span>
						<span class="pull-right"><i class="glyphicon glyphicon-volume-up"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">今日发布：{{$toArt}}</span>
						<span class="pull-right"><i class="glyphicon glyphicon-volume-up"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-success">
				<div class="panel-heading">
					<div class="row">
					    <div class="col-xs-9">
					        <div class="huge">评论数量</div>
					    </div>
					</div>
				</div>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">评论总量：{{$allCom}}</span>
						<span class="pull-right"><i class="glyphicon glyphicon-volume-up"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
				<a href="#">
					<div class="panel-footer">
						<span class="pull-left">今日发布：{{$toCom}}</span>
						<span class="pull-right"><i class="glyphicon glyphicon-volume-up"></i></span>
						<div class="clearfix"></div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
</body>
</html>
