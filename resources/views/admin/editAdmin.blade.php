<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$('#myTab a').click(function (e) {
e.preventDefault();
$(this).tab('show');
});
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div style="padding: 10px;" class="col-sm-4">
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="home" data-toggle="tab" role="tab" id="home-tab" href="#home">修改信息</a>
					</li>
					<li role="presentation">
						<a aria-controls="profile" data-toggle="tab" id="profile-tab" role="tab" href="#profile">修改密码</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent" style="padding:10px">
					<div aria-labelledby="home-tab" id="home" class="tab-pane fade active in" role="tabpanel">
						<form class="form-horizontal" method="POST" action="{{ url('posts/getUserInfo') }}">
							<input type="hidden" name="_token" value="{{csrf_token()}}">
							<div class="form-group">
								<label for="name" class="col-sm-4 control-label">昵称</label>
								<div class="col-sm-8">
									<input type="name" class="form-control" id="name" name="name" value="{{ Auth::user()->name }}"></div>
							</div>
							<div class="form-group">
								<label for="mywords" class="col-sm-4 control-label">个性签名</label>
								<div class="col-sm-8">
									<textarea class="form-control" rows="5" id="mywords" name="info">{{ Auth::user()->info }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-9 col-sm-3">
									<input type="submit" class="btn btn-info btn-lg" value="修改"></div>
							</div>
						</form>
					</div>
					<div aria-labelledby="profile-tab" id="profile" class="tab-pane fade " role="tabpanel">
						<div class="panel-heading">重设密码</div>
						<div class="panel-body text-danger">
							我们会给您的邮箱发送一封邮件，通过邮件链接可以重设密码。点击 <a class="btn btn-sm btn-danger" href="{{ url('reset/email') }}" target="_top">重置密码</a>
						</div>
					</div>
				</div>
			</div>
			@include('errors.errorinfo')
		</div>
	</div>

</body>
</html>