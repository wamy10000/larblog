<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link href="{{ asset('public/admin/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" media="screen">
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
@include('editor::head')
</head>
<body>
<div class="container-fluid">
<h4>文章发布</h4>
@include('errors.errorinfo')
<form class="form-horizontal" method="POST" action="{{url('yhsystem/article')}}">
	{!! csrf_field() !!}
	<div class="form-group">
		<label for="title" class="col-sm-1 control-label">标题</label>
		<div class="col-sm-4"><input type="title" class="form-control" id="title" name="title"></div>
		<label for="keyword" class="col-sm-1 control-label">关键字</label>
		<div class="col-sm-2"><input type="keyword" class="form-control" id="keyword" name="keyword"></div>
		<label for="name" class="col-sm-1 control-label">栏目</label>
		<div class="col-sm-2">
		<select name="cate_id" class="form-control" id='classify'>
			@foreach($cates as $cate)
			<option value="{{$cate['id']}}">{{ $cate['html'].$cate["cate_name"]}}</option>
			@endforeach
		</select>
		</div>
		
	</div>
	<div class="form-group">
		<label for="description" class="col-sm-1 control-label">摘要</label>
		<div class="col-sm-10">
			<textarea class="form-control" rows="1" id="description" name="description"></textarea>
		</div>
	</div>
	<div class="form-group">
		<label for="status" class="col-sm-1 control-label">状态</label>
		<div class="col-sm-2">
			<select name="status" class="form-control" id="status">
				<option value=0>普通文章</option>
				<option value=1>推荐</option>
				<option value=2>置顶</option>
			</select>
		</div>
		
		<label for="click" class="col-sm-1 control-label">点击</label>
		<div class="col-sm-1">
			<input type="click" value="100" class="form-control" id="click" name="click">
		</div>
		
		<label for="dtp_input2" class="col-sm-1 control-label">发布日期</label>
		<div class="input-group date form_date col-sm-4" data-date="" data-date-format="yyyy-mm-dd hh:ii:ss" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd hh:ii:ss">
		    <input class="form-control" size="16" type="text" value="" readonly>
		    <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
		</div>
		<input type="hidden" id="dtp_input2" value="" name="published_at" />
		
	</div>
	<div class="form-group">
		<label for="tags" class="col-sm-1 control-label">标签</label>
		<div class="col-sm-8">
			<select name="tags[]" style="width:500px" class="form-control" id="tags" multiple>
				@foreach($tags as $k=>$tag)
				<option value="{{$k}}">{{$tag}}</option>
				@endforeach
			</select>
		</div>
		<div class="col-sm-2"><button class="btn btn-success">发表</button>&nbsp;<a class="btn btn-danger" href="{{url('yhsystem/article')}}">返回</a></div>
	</div>
	
	
	<div class="form-group">
		<label for="content" class="col-sm-1 control-label">正文</label>

		<div class="col-sm-10 editor">
			<textarea id='myEditor' class="form-control" rows="20" id="content" name="content"></textarea>
		</div>
	</div>
</form>
</div>
</body>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/bootstrap-datetimepicker.min.js') }}" charset="UTF-8"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/lang/bootstrap-datetimepicker.zh-CN.js') }}" charset="UTF-8"></script>
<script type="text/javascript">
	$('.form_date').datetimepicker({
        language:  'zh-CN'
    });
    $('select').select2();
</script>
</html>