<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link href="./public/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$(function(){
	$('[data-toggle=tooltip]').tooltip();
	$('.btn-update').click(function(){
		var sta = $(this).attr('status');
		var content = $(this).parent('td').siblings().eq(1).text();
		var zan = $(this).parent('td').siblings().eq(2).text();
		$('#mywords').text(content);
		$('#zan').attr('value',zan);
		$('.form-update').attr('action',"{{url('yhsystem/talk/')}}/"+sta);
	});
	$('.btn-del').click(function(){
		var del = $(this).attr('status');
		$('.form-del').attr('action',"{{url('yhsystem/talk/')}}/"+del);
	});
})
</script>
<style type="text/css">
.pd{padding: 10px}
</style>
</head>
<body>
<div class="container-fluid">
	<div class="pd">
	@include('errors.errorinfo')
	<ul role="tablist" class="nav nav-tabs" id="myTab">
		<li class="active" role="presentation">
			<a>说说管理</a>
		</li>
		<li class="pull-right">
			<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTalk">添加说说</button>
		</li>
	</ul>
	</div>
	<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>说说</th>
			<th>赞</th>
			<th>发布时间</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		@foreach($talk as $v)
		<tr>
			<td>{{$v->id}}</td>
			<td>{{$v->content}}</td>
			<td>{{$v->zan}}</td>
			<td>{{$v->updated_at}}</td>			
			<td><button type="button" status="{{$v->id}}" class="btn btn-info btn-sm btn-update" data-toggle="modal" data-target="#editTalk">编辑</button>&nbsp;<button type="button" class="btn btn-danger btn-sm btn-del" status="{{$v->id}}" data-toggle="modal" data-target="#del">删除</button></td>
		</tr>
		@endforeach
	</tbody>
	</table>
	<div class="pull-right" style="padding-right: 100px">
		<nav>
			{!! $talk->render() !!}
		</nav>
	</div>
</div>

<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">确定删除这条说说？<br><i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;<span class='text-danger'>相关评论和点赞信息会被一起删除</span></div>
			<div class="modal-footer">
			<form class="form-del" action="" method="POST">
				{!! csrf_field() !!}
				<input type="hidden" name="_method" value="DELETE">
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</form>
				
			</div>
		</div>
	</div>
</div>
<!-- 发布说说提示框 -->
<div class="modal fade bs-example-modal-lg" id="addTalk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">发布说说</h4>
			</div>
			<form action="{{url('yhsystem/talk')}}" method="POST"  class="form-horizontal">
			{!! csrf_field() !!}
			<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-12">
							<textarea class="form-control" rows="10" id="content" name="content" placeholder="支持Markdown语法"></textarea>
							<p class="text-danger"> <i class="glyphicon glyphicon-exclamation-sign"></i> 支持<b class="text-primary">markdown语法</b>，详情参考：<a href="https://laravist.com/discussion/21" target="_blank">查看</a></p>
						</div>
					</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>

			</div>
			</form>
		</div>
	</div>
</div>

<!-- 修改说说提示框 -->
<div class="modal fade bs-example-modal-lg" id="editTalk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">修改说说</h4>
			</div>
			<form action="" method="POST"  class="form-horizontal form-update">
			{!! csrf_field() !!}
			<input type="hidden" name="_method" value="PATCH">
			<div class="modal-body">
				<div class="form-group">
					<label for="mywords" class="col-sm-1 control-label">说说</label>
					<div class="col-sm-10">
						<textarea class="form-control" rows="10" id="mywords" name="content"></textarea>
						<p class="text-danger"> <i class="glyphicon glyphicon-exclamation-sign"></i> 支持<b class="text-primary">markdown语法</b>，详情参考：<a href="https://laravist.com/discussion/21" target="_blank">查看</a></p>
					</div>						
				</div>
				<div class="form-group">
					<label for="zan" class="col-sm-1 control-label">赞</label>
					<div class="col-sm-10">
						<input type="text" name="zan" value="" id="zan">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>

			</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>