<!DOCTYPE html>
<html lang="zh_CN" style="overflow: hidden;">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="chrome=1,IE=edge" />
	<meta name="renderer" content="webkit|ie-comp|ie-stand" />
	<meta charset="utf-8" />
	<title>有聊后台管理系统</title>
	<meta name="description" content="This is page-header (.page-header &gt; h1)" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link href="{{ asset('public/admin/statics/simpleboot/themes/flat/theme.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/admin/statics/simpleboot/css/simplebootadmin.css') }}" rel="stylesheet" />
	<link href="{{ asset('public/admin/statics/simpleboot/font-awesome/4.2.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<!--[if IE 7]>
	<link rel="stylesheet" href="{{ asset('public/admin/statics/simpleboot/font-awesome/4.2.0/css/font-awesome-ie7.min.css') }}">
	<![endif]-->
	<link rel="stylesheet" href="{{ asset('public/admin/statics/simpleboot/themes/flat/simplebootadminindex.min.css?') }}" />
	<!--[if lte IE 8]>
	<link rel="stylesheet" href="{{ asset('public/admin/statics/simpleboot/css/simplebootadminindex-ie.css?') }}" />
	<![endif]-->
	<style>
.navbar .nav_shortcuts .btn{margin-top: 5px;}

/*-----------------导航hack--------------------*/
.nav-list>li.open{position: relative;}
.nav-list>li.open .back {display: none;}
.nav-list>li.open .normal {display: inline-block !important;}
.nav-list>li.open a {padding-left: 7px;}
.nav-list>li .submenu>li>a {background: #fff;}
.nav-list>li .submenu>li a>[class*="fa-"]:first-child{left:20px;}
.nav-list>li ul.submenu ul.submenu>li a>[class*="fa-"]:first-child{left:30px;}
/*----------------导航hack--------------------*/
</style>
</head>
<body style="min-width:900px;" screen_capture_injected="true">
	<div id="loading"> <i class="loadingicon"></i>
		<span>正在加载...</span>
	</div>
	<div id="right_tools_wrapper">
		<!--<span id="right_tools_clearcache" title="清除缓存" onclick="javascript:openapp('/admin/setting/clearcache.html','right_tool_clearcache','清除缓存');"> <i class="fa fa-trash-o right_tool_icon"></i>
	</span>
	-->
		<span id="refresh_wrapper" title="刷新当前页">
			<i class="fa fa-refresh right_tool_icon"></i>
		</span>
	</div>
<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a href="{{ url('yhsystem')}}" class="brand">
				<small>
					<img src="{{ asset('public/admin/statics/images/icon/logo-18.png') }}" />
					有聊后台管理系统
				</small>
			</a>
			<div class="pull-left nav_shortcuts">
				<a class="btn btn-small btn-success" href="javascript:openapp('{{url('yhsystem/cate')}}','index_termlist','栏目管理');" title="栏目管理">
					<i class="fa fa-th"></i>
				</a>
				<a class="btn btn-small btn-info" href="javascript:openapp('{{url('yhsystem/article')}}','index_postlist','文章管理');" title="文章管理">
					<i class="fa fa-pencil"></i>
				</a>
				<a class="btn btn-small btn-warning" href="/" title="前台首页" target="_blank">
					<i class="fa fa-home"></i>
				</a>
				<a class="btn btn-small btn-danger" href="javascript:openapp('/admin/setting/clearcache.html','index_clearcache','清除缓存');" title="清除缓存">
					<i class="fa fa-trash-o"></i>
				</a>
			</div>
			<ul class="nav simplewind-nav pull-right">
				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<img class="nav-user-photo" src="{{ asset('public/admin/statics/images/icon/logo-18.png') }}" alt="demo" />
						<span class="user-info">
							<small>欢迎,</small>
							{{Auth::user()->name}}
						</span>
						<i class="fa fa-caret-down"></i>
					</a>
					<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
						<li>
							<a href="javascript:openapp('{{url('yhsystem/webSet')}}','site','站点管理');">
								<i class="fa fa-cog"></i>
								站点管理
							</a>
						</li>
						<li>
							<a href="javascript:openapp('{{url('yhsystem/getUserInfo')}}','editAdmin','个人资料');">
								<i class="fa fa-user"></i>
								个人资料
							</a>
						</li>
						<li class="divider"></li>
						<li>
							<a href="{{url('auth/logout')}}">
								<i class="fa fa-sign-out"></i>
								退出
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="main-container container-fluid">
	<div class="sidebar" id="sidebar">
		<!-- <div class="sidebar-shortcuts" id="sidebar-shortcuts"></div>
	-->
	<div id="nav_wraper">
		<ul class="nav nav-list">
			<li>
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-cogs normal"></i>
					<span class="menu-text normal">设置</span> <b class="arrow fa fa-angle-right normal"></b>
					<i class="fa fa-reply back"></i>
					<span class="menu-text back">返回</span>
				</a>
				<ul class="submenu">
					<li>
						<a href="javascript:openapp('{{url('yhsystem/getUserInfo')}}','253Admin','个人信息');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">个人信息</span>
						</a>
					</li>
					<li>
						<a href="javascript:openapp('{{url('yhsystem/webSet')}}','site','网站信息');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">网站信息</span>
						</a>
					</li>
					<li>
						<a href="javascript:openapp('{{url('yhsystem/stemp')}}','528Admin','SMTP配置');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">邮箱配置</span>
						</a>
						
					</li>
				</ul>
			</li>
			<li>
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-group normal"></i>
					<span class="menu-text normal">用户管理</span>
					<b class="arrow fa fa-angle-right normal"></b>
					<i class="fa fa-reply back"></i>
					<span class="menu-text back">返回</span>
				</a>
				<ul class="submenu">
					<li>
						<a href="javascript:openapp('{{url('yhsystem/myUser')}}','myuser','本站用户');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">本站用户</span>
						</a>
						
					</li>
					<li>
						<a href="javascript:openapp('{{url('yhsystem/myRole')}}','controladmin','管理员');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">管理员</span>							
						</a>						
					</li>
				</ul>
			</li>
			<li>
				<a href="javascript:openapp('{{url('yhsystem/cate')}}','indexmenu','栏目管理');">
					<i class="fa fa-list normal"></i>
					<span class="menu-text normal">栏目管理</span>
					<b class="arrow fa fa-angle-right normal"></b>					
				</a>
			</li>
			<li>
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-th normal"></i>
					<span class="menu-text normal">内容管理</span>
					<b class="arrow fa fa-angle-right normal"></b>
					<i class="fa fa-reply back"></i>
					<span class="menu-text back">返回</span>
				</a>
				<ul class="submenu">
					<li>
						<a href="javascript:openapp('{{url('yhsystem/article')}}','articleAdmin','文章管理');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">文章管理</span>
						</a>
					</li>
					<li>
						<a href="javascript:openapp('{{url('yhsystem/talk')}}','talkAdmin','说说管理');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">说说管理</span>
						</a>
					</li>
					<li>
						<a href="javascript:openapp('{{url('yhsystem/comment')}}','557Comment','评论管理');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">评论管理</span>
						</a>
					</li>
					<li>
						<a href="javascript:openapp('{{url('yhsystem/article/dust')}}','302Portal','文章回收');" class="dropdown-toggle">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">回收站</span>
						</a>
						
					</li>
				</ul>
			</li>
			<li>
				<a href="#" class="dropdown-toggle">
					<i class="fa fa-cloud normal"></i>
					<span class="menu-text normal">扩展工具</span>
					<b class="arrow fa fa-angle-right normal"></b>
					<i class="fa fa-reply back"></i>
					<span class="menu-text back">返回</span>
				</a>
				<ul class="submenu">
					<li>
						<a href="javascript:openapp('{{url('yhsystem/link')}}','adAdmin','友情链接');">
							<i class="fa fa-caret-right"></i>
							<span class="menu-text">友情链接</span>
						</a>
					</li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<div class="main-content">
	<div class="breadcrumbs" id="breadcrumbs">
		<a id="task-pre" class="task-changebt">←</a>
		<div id="task-content">
			<ul class="macro-component-tab" id="task-content-inner">
				<li class="macro-component-tabitem noclose" app-id="0" app-url="/admin/main/index.html" app-name="首页">
					<span class="macro-tabs-item-text">首页</span>
				</li>
			</ul>
			<div style="clear:both;"></div>
		</div>
		<a id="task-next" class="task-changebt">→</a>
	</div>
	<div class="page-content" id="content">
		<iframe src="{{ action('Admin\AdminController@main') }}" style="width:100%;height: 100%;min-width: 800px;" frameborder="0" id="appiframe-0" class="appiframe" scrolling="yes"></iframe>
	</div>
</div>
</div>
<script src="{{ asset('public/admin/statics/js/jquery.js') }}"></script>
<script src="{{ asset('public/admin/statics/simpleboot/bootstrap/js/bootstrap.min.js') }}"></script>
<script>
var b = $("#sidebar").hasClass("menu-min");
var a = "ontouchend" in document;
$(".nav-list").on("click",
	function(g) {
		var f = $(g.target).closest("a");
		if (!f || f.length == 0) {
			return
		}
		if (!f.hasClass("dropdown-toggle")) {
			if (b && "click" == "tap"
					&& f.get(0).parentNode.parentNode == this) {
				var h = f.find(".menu-text").get(0);
				if (g.target != h && !$.contains(h, g.target)) {
					return false
				}
			}
			return
		}
		var d = f.next().get(0);
		if (!$(d).is(":visible")) {
			var c = $(d.parentNode).closest("ul");
			if (b && c.hasClass("nav-list")) {
				return
			}
			c.find("> .open > .submenu").each(
					function() {
						if (this != d
								&& !$(this.parentNode).hasClass(
										"active")) {
							$(this).slideUp(150).parent().removeClass(
									"open")
						}
					})
		} else {
		}
		if (b && $(d.parentNode.parentNode).hasClass("nav-list")) {
			return false;
		}
		$(d).slideToggle(150).parent().toggleClass("open");
		return false;
	});
</script>
<script src="{{ asset('public/admin/statics/js/index.js') }}"></script>
</body>
</html>