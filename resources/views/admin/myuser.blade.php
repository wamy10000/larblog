<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<meta name="_token" content="{{csrf_token()}}">
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$(function(){
	$('[data-toggle=tooltip]').tooltip();
	$('.tbtn').click(function(){
		$info = $(this).attr('mystatus');
		$('input[name="id"]').attr('value',$info);
	})	
});

</script>
</head>
<body>
<div class="container-fluid">
	<table class="table table-hover">
	<thead>
		<tr>
			<th>ID</th>
			<th>邮箱地址</th>
			<th>昵称</th>
			<th>头像</th>
			<th>最后登录时间</th>
			<th>状态</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($myusers as $user )
		<tr>
			<td>{{$user['id']}}</td>
			<td>{{$user['email']}}</td>
			<td>{{$user['name']}}</td>
			<td>
			<a data-placement="right" data-toggle="tooltip" href="#" data-html='1' data-original-title="<img src='{{asset($user["pic"])}}' width='100' />"><img src='{{asset($user["pic"])}}' width="20" /></a>
			</td>
			<td>{{$user['updated_at']}}</td>
			<td>
				@if( $user['status'] )
				已启用
				@else
				已拉黑
				@endif
			</td>
			<td><button mystatus="{{$user['id']}}" type="button" class="btn btn-danger btn-sm tbtn" data-toggle="modal" data-target="#lahei">拉黑</button> <button mystatus="{{$user['id']}}" type="button" class="btn btn-success btn-sm tbtn" data-toggle="modal" data-target="#qiyong">启用</button></td>
		</tr>
		@endforeach
	</tbody>
	</table>
	<div class="pull-right" style="padding-right: 100px">

		<nav>
			{!! $myusers->render() !!}
		</nav>
	</div>
</div>

<!-- l拉黑提示框 -->
<div class="modal fade bs-example-modal-sm" id="lahei" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">提示信息</h4>
			</div>
			<div class="modal-body">确定拉黑该用户？</div>
			<div class="modal-footer">
			<form action="{{url('posts/myUser')}}" method="POST">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="status" value="0">
				<input type="hidden" name="id" value="">
				<button type="submit" class="btn btn-success">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</form>				
				

			</div>
		</div>
	</div>
</div>
<!-- 启用提示框 -->
<div class="modal fade bs-example-modal-sm" id="qiyong" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">提示信息</h4>
			</div>
			<div class="modal-body">确定启用该用户？</div>
			<div class="modal-footer">
				<form action="{{url('posts/myUser')}}" method="POST">
					<input type="hidden" name="_token" value="{{csrf_token()}}">
					<input type="hidden" name="status" value="1">
					<input type="hidden" name="id" value="">
					<button type="submit" class="btn btn-success">确认</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				</form>	
				

			</div>
		</div>
	</div>
</div>
</body>
</html>
