<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$('#myTab a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div style="padding: 10px;" class="col-sm-12">
			@include('errors.errorinfo')
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="allad" data-toggle="tab" role="tab" id="allad-tab" href="#allad">所有评论</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent" style="padding:10px">
					<div aria-labelledby="allad-tab" id="allad" class="tab-pane fade active in" role="tabpanel">
						<form action="{{url('setcomment')}}" method="post">
						{!! csrf_field() !!}
						<table class="table">
							<thead>
								<tr>
									<th width="5%">选择</th>
									<th width="5%">ID</th>
									<th width="10%">所属文章</th>
									<th width="10%">评论者</th>
									<th width="50%">评论内容</th>
									<th width="10%">
									    <button type="submit" class="btn btn-sm btn-info btn-shenhe">审核</button>
									</th>
									<th width="5%">操作</th>
								</tr>
							</thead>
							<tbody>
								@foreach($info as $v)
								<tr>
									<td><input name='ckb[]' type="checkbox" value="{{$v->id}}"></td>
									<td>{{$v->id}}</td>
									<td>{{ App\Article::where('id',$v->art_id)->pluck('title') }}</td>
									<td>{{ App\User::where('id',$v->user_id)->pluck('name') }}</td>
									<td>{{$v->content}}</td>
									<td>
                                    @if($v->check)
                                    <span class="text-success">已审核</span>
                                    @else
                                    <span class="text-danger">未审核</span>
                                    @endif
                                    </td>
									<td><button status="{{$v->id}}" type="button" class="btn btn-danger btn-sm btn-del" data-toggle="modal" data-target="#del">删除</button></td>
								</tr>
								@endforeach
								<tr>
									<td colspan="2"><input type="checkbox" id="checkAll">&nbsp;全选</td>
								</tr>
							</tbody>
						</table>
						</form>
						<div class="pull-right" style="padding-right: 100px">
							<nav>
								{!! $info->render() !!}
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">确定删除这条评论？<br><i class="glyphicon glyphicon-warning-sign"></i>&nbsp;&nbsp;<span class='text-danger'>相关评论回复会被一起删除</span></div>
			<div class="modal-footer">
				<form action="" method="POST" class="form-del">
				
				<input type="hidden" name="_method" value="DELETE">
				{!! csrf_field() !!}
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				</form>

			</div>
		</div>
	</div>
</div>

</body>
<script type="text/javascript">
//全选or全不选
$("#checkAll").bind({
	click:function(){
		if(this.checked){
	        $('[type=checkbox]').prop('checked',true);
	    }else{
	        $('[type=checkbox]').prop('checked',false);
	    }
	}
});
//删除评论提交地址
$(".btn-del").click(function(){
	delId = $(this).attr('status');
	$('.form-del').attr('action',"{{url('yhsystem/comment')}}/"+delId);
});
</script>
</html>