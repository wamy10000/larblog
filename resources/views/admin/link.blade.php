<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$(function(){
	
	$('#myTab a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
	$('.btn-del').click(function(){
		var delid = $(this).attr('status');
		$('.form-del').attr('action',"{{url('yhsystem/link/')}}/"+delid);
	});
	$('.btn-edit').click(function(){
		var editid = $(this).attr('editid');
		var name = $(this).parent('td').siblings().eq(1).text();
		var link = $(this).parent('td').siblings().eq(2).text();
		$('.form-edit').attr('action',"{{url('yhsystem/link/')}}/"+editid);
		$("#linkname2").attr('value',name);
		$('#link2').attr('value',link);
	});
});
</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div style="padding: 10px;" class="col-sm-12">
			@include('errors.errorinfo')
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="allad" data-toggle="tab" role="tab" id="allad-tab" href="#allad">所有链接</a>
					</li>
					<li class="pull-right">
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#addAd">添加友情链接</button>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent" style="padding:10px">
					<div aria-labelledby="allad-tab" id="allad" class="tab-pane fade active in" role="tabpanel">
						<table class="table">
							<thead>
								<tr>
									<th>ID</th>
									<th>链接名称</th>
									<th>链接地址</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								@foreach($links as $link)
								<tr>
									<td>{{$link->id}}</td>
									<td>{{$link->name}}</td>
									<td>{{$link->link}}</td>
									<td><button type="button" class="btn btn-info btn-sm btn-edit" data-toggle="modal" data-target="#editAd" editid="{{$link->id}}">编辑</button>&nbsp;<button type="button" class="btn btn-danger btn-sm btn-del"  status="{{$link->id}}" data-toggle="modal" data-target="#del">删除</button></td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">警告</h4>
			</div>
			<div class="modal-body">确定删除这条链接？</div>
			<div class="modal-footer">
			<form class="form-del" method="POST" action="">
				{!! csrf_field() !!}
				<input type="hidden" name="_method" value="DELETE">
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</form>
			</div>
		</div>
	</div>
</div>
<!-- 添加广告提示框 -->
<div class="modal fade bs-example-modal-lg" id="addAd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加友情链接</h4>
			</div>
			<form action="{{url('yhsystem/link')}}" method="POST"  class="form-horizontal">
				{!! csrf_field() !!}
				<div class="modal-body">
					<div class="form-group">
						<label for="linkname1" class="col-sm-2 control-label">连接名称</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="name" id="linkname1" placeholder="链接名称"></div>
					</div>
					<div class="form-group">
						<label for="link1" class="col-sm-2 control-label">链接地址</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="link" id="link1" placeholder="链接地址"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">确认</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- 修改广告提示框 -->
<div class="modal fade bs-example-modal-lg" id="editAd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">修改链接</h4>
			</div>
			<form action="" method="POST"  class="form-horizontal form-edit">
				<div class="modal-body">
				{!! csrf_field() !!}
					<input type="hidden" name="_method" value="PATCH">
					<div class="form-group">
						<label for="linkname2" class="col-sm-2 control-label">连接名称</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="name" id="linkname2" placeholder="链接名称"></div>
					</div>
					<div class="form-group">
						<label for="link2" class="col-sm-2 control-label">链接地址</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" name="link" id="link2" placeholder="链接地址"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger">确认</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>