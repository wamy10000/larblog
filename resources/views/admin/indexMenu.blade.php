<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>asd</title>
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.2/css/bootstrap.min.css">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/admin/js/functions.js') }}"></script>
<script type="text/javascript">
$(function(){
	$('[data-toggle=tooltip]').tooltip();
	$('.edbtn').click(function(){
		$status = $(this).attr('mystatus');
		$pid = $(this).parent().parent().attr('pid');
		$name = $(this).parent().siblings('td').children('span').text();
		$sort = $(this).parent().siblings('td').eq(3).children('input').val();
		$('.col-sm-7').children('input[name="cate_name"]').attr('value',$name);
		$('.col-sm-7').children('input[name="sort"]').attr('value',$sort);
		$('.col-sm-7').children('input[name="id"]').attr('value',$status);
		$('.col-sm-7').children('select').children('option').attr('selected',false);
		$('.col-sm-7').children('select').children('option[value='+$pid+']').attr('selected','selected');
	});
	$('.delbtn').click(function(){
		$delid = "{{url('yhsystem/cate/')}}/"+$(this).parent().siblings('td').eq(1).text(); 
		$('.delform').attr('action',$delid);
	});
});
</script>
<style type="text/css">
i{cursor: pointer;}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
		@include('errors.errorinfo')
			<div style="padding: 10px;" class="col-sm-12">
				<ul role="tablist" class="nav nav-tabs" id="myTab">
					<li class="active" role="presentation">
						<a aria-expanded="true" aria-controls="control" data-toggle="tab" role="tab" id="control-tab" href="#control">前台栏目</a>
					</li>
					<li class="pull-right">
						<button type="button" class="btn btn-success" data-toggle="modal" data-target="#add">添加栏目</button>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent" style="padding:10px">
					<div aria-labelledby="control-tab" id="control" class="tab-pane fade active in" role="tabpanel">
					<form action="{{url('posts/sortCate')}}" method="POST">
					{!! csrf_field() !!}
						<table class="table table-hover">
							<thead>
								<tr>
									<th width="5%" class="text-center">折叠</th>
									<th width="5%">ID</th>
									<th width="60%">菜单名称</th>
									<th width="10%">									
										<button class="btn-success btn-sm" type="submit">排序</button>						
									</th>
									<th width="20%">管理操作</th>
								</tr>
							</thead>
							<tbody>
								@foreach($info as $getinfo)
								<tr pid='{{$getinfo->p_id}}' uid='{{$getinfo->id}}'>
									<td class="text-center"><i class="glyphicon glyphicon-plus open1"></i></td>
									<td>{{$getinfo['id']}}</td>
									<td>{{ $getinfo['html']}}<span>{{$getinfo["cate_name"]}}</span></td>
									<td><input type="text" class="form-control" style="height:30px;width:50px" value="{{$getinfo['sort']}}" name="{{$getinfo['id']}}"></td>
									<td><button type="button" mystatus="{{$getinfo['id']}}" class="btn btn-info btn-sm edbtn" data-toggle="modal" data-target="#edit">修改</button> <button type="button" class="btn btn-danger btn-sm delbtn" data-toggle="modal" data-target="#del">删除</button></td>
								</tr>
								@endforeach

							</tbody>
						</table>
						</form>	
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- 删除提示框 -->
<div class="modal fade bs-example-modal-sm" id="del" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">提示信息</h4>
			</div>
			<div class="modal-body">确定删除该栏目？(注意：该栏目下的子栏目也会一并删除！！)</div>
			<div class="modal-footer">
			<form class="delform" method="POST" action="">
				{!! csrf_field() !!}
				<input type="hidden" name="_method" value="delete">
				<button type="submit" class="btn btn-danger">确认</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			</form>				
			</div>
		</div>
	</div>
</div>

<!--添加栏目-->
<div id="add" class="modal fade bs-example-modal-lg row" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">添加栏目</h4>
			</div>
			<form class="form-horizontal" method="POST" action="{{url('yhsystem/cate/add')}}">
			{!! csrf_field() !!}
			<div class="modal-body">
				<div class="form-group">
					<label for="name" class="col-sm-2 control-label">栏目名称</label>
					<div class="col-sm-8">
						<input type="name" class="form-control" id="name" name="cate_name"></div>
				</div>
				<div class="form-group">
					<label for="descrip" class="col-sm-2 control-label">排序</label>
					<div class="col-sm-8">
						<input type="descrip" class="form-control" name="sort" id="descrip" value="1">
					</div>
				</div>
				<div class="form-group">
					<label for="myindex" class="col-sm-2 control-label">所属栏目</label>
					<div class="col-sm-8">
						<select class="form-control" type="myindex" id="myindex" name="p_id">
							<option value="0">顶级栏目</option>
							@foreach($info as $getinfo)
							<option value="{{$getinfo['id']}}">{{$getinfo['html'].$getinfo['cate_name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-6">
						<input type="submit" class="btn btn-info btn-lg" value="修改"><button type="button" class="btn btn-default btn-lg" data-dismiss="modal">取消</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<!--修改栏目-->
<div id="edit" class="modal fade bs-example-modal-lg row" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">修改栏目</h4>
			</div>
			<form class="form-horizontal" method="POST" action="{{url('yhsystem/cate')}}">
			{!! csrf_field() !!}
			<div class="modal-body">
				<div class="form-group">				
					<label for="name" class="col-sm-2 control-label">栏目名称</label>
					<div class="col-sm-7">
						<input type="hidden" name="id">
						<input type="name" class="form-control" id="name" name="cate_name"></div>
				</div>
				<div class="form-group">
					<label for="descrip" class="col-sm-2 control-label">排序</label>
					<div class="col-sm-7">
						<input type="descrip" class="form-control" name="sort" id="descrip" value="1">
					</div>
				</div>
				<div class="form-group">
					<label for="myindex" class="col-sm-2 control-label">所属栏目</label>
					<div class="col-sm-7">
						<select class="form-control" type="myindex" id="myindex" name="p_id">
							<option value="0">顶级栏目</option>
							@foreach($info as $getinfo)
							<option value="{{$getinfo['id']}}">{{$getinfo['html'].$getinfo['cate_name']}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-6">
						<input type="submit" class="btn btn-info btn-lg" value="修改"><button type="button" class="btn btn-default btn-lg" data-dismiss="modal">取消</button>
					</div>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$('#myTab a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});

//展开菜单相关代码
$('tbody>tr[pid!=0]').hide();
$(".open1").bind({
	click:function(){
		var index = $(this).parents('tr').attr('uid');
		$(this).toggleClass("glyphicon-plus");
		$(this).toggleClass("glyphicon-minus");
		$("tr[pid="+index+"]").toggle(500);
	}
});
</script>
</body>
</html>

