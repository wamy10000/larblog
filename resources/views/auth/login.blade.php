<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>有聊－－会员登录</title>
@include('public.header')
<style type="text/css">
	body{color:#fff;}
</style>
</head>
<body style="background-image: url({{ asset('public/images/hero.jpg') }});background-size: 100%;">
<a class="pull-right btn btn-danger btn-sm" href="/">回首页</a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<div class="row clearfix mt100">
				<div class="col-md-4 column">
				</div>
				<div class="col-md-4 column animated slideInDown">
					<h2>登录有聊</h2>
					<form class="form-horizontal" role="form" method="POST" action="{{ route('loginPost') }}">
                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							 <label for="exampleInputEmail1">邮箱:</label><input class="form-control" id="exampleInputEmail1" type="email" name="email" placeholder="请输入邮箱地址" value="{{old('email')}}"/>
						</div>
						<div class="form-group">
							 <label for="exampleInputPassword1">密码:</label><input class="form-control" id="exampleInputPassword1" type="password" name="password" placeholder="请输入密码" />
						</div>
						<div class="checkbox">
							 <label><input type="checkbox" name="remember">记住我</label><a href="{{ url('/password/email') }}" class="pull-right">忘记密码？</a>
						</div>
						<button type="submit" class="btn btn-success btn-block">登录</button>
						<a class="btn btn-danger btn-block" href="{{ url('/auth/register') }}">注册</a>
					</form>
				</div>
				<div class="col-md-4 column">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>出错了!</strong> 输入信息有误.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if( Session::has('errorInfo') )
					<div style="margin-top:50px;" class="alert alert-info alert-dismissible" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						提示信息!<br><strong>{{ Session::get('errorInfo') }}.</strong> 
					</div>
				    @endif
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</html>
