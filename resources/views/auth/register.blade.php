<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>有聊－－会员注册</title>
@include('public.header')
</head>
<body style="color:#F4655F;background-image: url({{ asset('public/images/hero.jpg') }});background-size: 100%;">
<a class="pull-right btn btn-danger btn-sm" href="/">回首页</a>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column animated slideInDown">
			<div class="row clearfix mt100">
				<div class="col-md-4 column">
				</div>
				<div class="col-md-4 column">

					<h2>
						注册一个账号<small>就是这么简单</small>
					</h2>

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							 <label for="exampleInputEmail1">邮箱:</label><input class="form-control" id="exampleInputEmail1" type="email" name="email" placeholder="请输入邮箱地址"/>
						</div>
						<div class="form-group">
							 <label for="examplename">昵称:</label><input class="form-control" id="examplename" type="name" name="name" placeholder="请输入昵称"/>
						</div>
						<div class="form-group">
							 <label for="exampleInputPassword1">密码:</label><input class="form-control" id="exampleInputPassword1" type="password" name="password" placeholder="请输入密码"/>
						</div>
						<div class="form-group">
							 <label for="exampleInputPassword2">确认密码:</label><input class="form-control" id="exampleInputPassword2" type="password" name="password_confirmation" placeholder="请输入密码"/>
						</div>

						<button type="submit" class="btn btn-danger btn-block">注册</button>
					</form>
				</div>
                <div class="col-md-4 column">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</html>
