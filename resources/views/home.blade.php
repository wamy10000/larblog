@extends('app')
@section('title')
个人中心
@stop
@section('header')
<link rel="stylesheet" type="text/css" href="{{ asset('public/css/home.css') }}">
@stop

@section('cate')
@include('public.cate')
@stop

@section('content')

<div class="container main-container">
	@include('errors.errorinfo')
	<div class="col-md-2 sidebar">
		<div class="row">
			<!-- uncomment code for absolute positioning tweek see top comment in css -->
			<div class="absolute-wrapper"> </div>
			<!-- Menu -->
			<div class="side-menu">
				<nav class="navbar navbar-default" role="navigation">
					<!-- Main Menu -->
					<div class="side-menu-container">
						<ul class="nav navbar-nav" role="tablist">
							<li role="presentation"><a href="#myprofile" aria-controls="myprofile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-dashboard"></span>个人信息</a></li>
							<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-dashboard"></span>资料修改</a></li>
							<li role="presentation"><a href="#mypic" aria-controls="mypic" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-plane"></span>头像修改</a></li>
							<li role="presentation"><a href="#mypass" aria-controls="mypass" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-plane"></span>密码重置</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</nav>
			</div>

		</div>
	</div>

	<div class="col-md-10">

		<div class="panel panel-default">

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active panel-info" id="myprofile">
					<div class="panel-heading">信息中心</div>
					<div class="panel-body">
						<ul class="list-group">
							<li class="list-group-item list-group-item-success"><div class="col-md-3">账号：</div>{{ Auth::user()->email }}</li>
							<li class="list-group-item list-group-item-info"><div class="col-md-3">昵称：</div>{{ Auth::user()->name }}</li>
							<li class="list-group-item list-group-item-warning"><div class="col-md-3">签名：</div>{{ Auth::user()->info }}</li>
							<li class="list-group-item list-group-item-danger"><div class="col-md-3">头像：</div><img src="{{ Auth::user()->pic }}"></li>
						</ul>
					</div>

				</div>
				<div role="tabpanel" class="tab-pane panel-danger" id="profile">
					<!-- Default panel contents -->
					<div class="panel-heading">个人信息</div>
					<div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>出错了!</strong> 输入信息有误.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
						<form class="form-horizontal" method="POST" action="{{action('Index\HomeController@store',[Auth::user()->id]) }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
							<label for="nickname" class="col-sm-2 control-label">昵称</label>
								<div class="col-sm-10">
									<input class="form-control" name="name" id="nickname" type="text" value="{{ $set['name'] }}" />
								</div>
							</div>
							<div class="form-group">
								<label for="info" class="col-sm-2 control-label">个性签名</label>
								<div class="col-sm-10">
									<textarea class="form-control" name="info" rows="3" id="info">{{ $set['info'] }}</textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-warning">修改</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane pd" id="mypic">
					<div class="row">
					<div class="col-lg-9">
						<div class="imageBox">
							<div class="thumbBox"></div>
							<div class="spinner" style="display: none">Loading...</div>
						</div>
						<div class="action">
							<!-- <input type="file" id="file" style=" width: 200px">-->
							<div class="new-contentarea tc"> <a href="javascript:void(0)" class="upload-img"><label for="upload-file">选择头像</label></a>
								<input type="file" class="" name="upload-file" id="upload-file" />
							</div>
						<input type="button" id="btnCrop"  class="Btnsty_peyton" value="裁剪">
						<input type="button" id="btnZoomIn" class="Btnsty_peyton" value="+"  >
						<input type="button" id="btnZoomOut" class="Btnsty_peyton" value="-" >
						</div>
						<div class="cropped"></div>
					</div>
					<div class="col-lg-3">
						<p class="text-danger" style="margin-top: 60px;">选择一张图片</p>
						<p>滚动滑轮可以控制图像大小</p>
						<p>鼠标拖拽可以选择图像位置</p>
						<p>点击裁剪后生成头像,点击上传提交修改</p>
						<form class="form-horizontal" method="POST" action="{{action('Index\HomeController@avatar') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input name="pic" id="ipicfile" type="hidden" value="">
							<button id="upfile" class="btn btn-danger">上传</button>
						</form>
					</div>
					</div>
				</div>
				<div role="tabpanel" class="tab-pane panel-info" id="mypass">
					<div class="panel-heading">重设密码</div>
					<div class="panel-body text-danger">
						我们会给您的邮箱发送一封邮件，通过邮件链接可以重设密码。点击 <a class="btn btn-sm btn-danger" href="{{ url('reset/email') }}">重置密码</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('footer')
<script src="{{ asset('public/js/cropbox.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(window).load(function() {
	var options =
	{
		thumbBox: '.thumbBox',
		spinner: '.spinner',
		imgSrc: '{{ Auth::user()->pic }}'
	}
	var cropper = $('.imageBox').cropbox(options);
	$('#upload-file').on('change', function(){
		var reader = new FileReader();
		reader.onload = function(e) {
			options.imgSrc = e.target.result;
			cropper = $('.imageBox').cropbox(options);
		}
		reader.readAsDataURL(this.files[0]);
		this.files = [];
	})
	$('#btnCrop').on('click', function(){
		var img = cropper.getDataURL();
		$('.cropped').html('');
		$('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:64px;margin-top:4px;border-radius:64px;box-shadow:0px 0px 12px #7E7E7E;" ><p>64px*64px</p>');
		$('.cropped').append('<img src="'+img+'" align="absmiddle" style="width:128px;margin-top:4px;border-radius:128px;box-shadow:0px 0px 12px #7E7E7E;"><p>128px*128px</p>');
		$('.cropped').append('<img id="picfile" src="'+img+'" align="absmiddle" style="width:180px;margin-top:4px;border-radius:180px;box-shadow:0px 0px 12px #7E7E7E;"><p>180px*180px</p>');
		var txt = $("#picfile").attr("src");
		$('#ipicfile').val(txt);
	})
	$('#btnZoomIn').on('click', function(){
		cropper.zoomIn();
	})
	$('#btnZoomOut').on('click', function(){
		cropper.zoomOut();
	})
	$('#upfile').on('click',function(){
		
		if(!txt){
			alert('请先选择图片并裁剪!');
		}
	})
});
</script>
@stop