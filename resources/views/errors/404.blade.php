<!DOCTYPE html>
<html>
    <head>
        <title>出错了.</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 52px;
                margin-bottom: 40px;
                color: #90C2EB;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">出错了！<br>您访问了一个未知的页面.<br>请返回<a href="/">首页</a></div>
            </div>
        </div>
    </body>
</html>
