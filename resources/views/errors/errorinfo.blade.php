@if( Session::has('success') )
	<div style="margin: 10px;" class="alert alert-success alert-dismissible col-sm-12" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		提示信息!<br><strong>{{ Session::get('success') }}.</strong> 
	</div>
@endif
@if( Session::has('error'))
	<div style="margin: 10px;" class="alert alert-danger alert-dismissible col-sm-12" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		错误信息!<br><strong>{{ Session::get('error') }}.</strong> 
	</div>
@endif
@if (count($errors) > 0)
	<div class="row alert alert-danger alert-dismissible" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	    <strong>出错了!</strong> 刚才的提交有误。<br><br>
	    <ul>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
	</div>
@endif
<script type="text/javascript">
	$('div.alert-dismissible').delay(4000).slideUp(300);
</script>