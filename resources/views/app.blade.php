<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="keywords" content="@yield('keywords','芒果小叨,闫海,有聊网,有聊')">
<meta name="description" content="@yield('description','芒果小叨,一个会点三脚猫功夫的伪php程序员。有聊网,你不可能感觉到无聊。')">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="baidu-site-verification" content="AXec4wSoH0" />
<title>@yield('title','有聊')--闫海的个人博客--芒果小叨</title>
@yield('beheader')
@include('public.header')
@yield('header')
<script>
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "//hm.baidu.com/hm.js?921ebae1ad049cd1c95a03b6a9434668";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
</script>
<script type="text/javascript">
$(function(){

$(document).pjax('a[target!=_blank]', '#pjax',{fragment:'#pjax', timeout:8000});

//$.pjax.reload('#pjax');
})
</script>
</head>
<body>
<div id="pjax">
@yield('indexwell')
<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><i class="glyphicon glyphicon-home"></i> 有聊</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active">
							<a href="/">首页</a>
						</li>
						@yield('cate')
					</ul>
					<form class="navbar-form navbar-left" role="search" method="POST" action="{{url('search')}}">
						<div class="form-group">
						{!! csrf_field() !!}
							<input class="form-control" type="text" placeholder="总得知道自己想要什么" name="contents"/>
						</div>
						<button type="submit" class="btn btn-default btn-danger">找刺激</button>
					</form>
					<ul class="nav navbar-nav navbar-right">
						<!--<li class="dropdown">
							<a id="newlogo" href="#" class="dropdown-toggle" data-toggle="dropdown">
								玩具<strong class="caret"></strong>
							</a>
							<ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/page/tree') }}" >摇钱树</a>
                                </li>
                                <li>
                                    <a href="{{ url('/page/spider') }}">疯狂的蜘蛛</a>
                                </li>
                                <li>
                                    <a href="{{ url('/page/shapes') }}">神奇的形状</a>
                                </li>
                                <li>
                                    <a href="{{ url('/page/cloth') }}">一块破布头</a>
                                </li>
                            </ul>
						</li>-->
						<li><a id="newlogo" href="{{url('page/timeline')}}">时光轴</a></li>
						
						<li class="dropdown">
						    @if(Auth::user())
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								{{ Auth::user()->name }} <strong class="caret"></strong>
							</a>
							<ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('home') }}">资料修改</a>
                                </li>
                                <li>
                                    <a href="{{ url('/auth/logout') }}">退出</a>
                                </li>
                            </ul>
							@else
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                登录 <strong class="caret"></strong>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ url('/auth/login') }}">登录</a>
                                </li>
                                <li>
                                    <a href="{{ url('/auth/register') }}">注册</a>
                                </li>
                            </ul>
                            @endif

						</li>
					</ul>
				</div>
			</nav>
			@yield('indexinfo')
		</div>
	</div>
</div>
@yield('content')	
</div>
@include('public.footer')
@yield('footer')
</body>
</html>
