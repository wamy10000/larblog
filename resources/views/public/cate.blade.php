@foreach($cate as $v)
@if(count($v->child))
	<li class="dropdown">
		
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			{{$v->cate_name}} <strong class="caret"></strong>
		</a>

		<ul class="dropdown-menu">
			@foreach($v->child as $vv)
			<li>
				<a href="{{url('lists'.$vv->id.'/')}}">{{$vv->cate_name}}</a>
			</li>
			@endforeach
		</ul>	
	</li>						
@else
	<li><a href="{{url('lists'.$v->id.'/')}}">{{$v->cate_name}}</a></li>
@endif							
@endforeach