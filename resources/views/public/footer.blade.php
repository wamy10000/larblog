@if(isset($webset))
<footer class="bs-docs-footer" style="background:rgba(0, 0, 0, 0) url(http://lumen.laravel.com/img/bright.jpg) no-repeat scroll center top;">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-1 col-md-4">
				<div class="music-player">
					<div class="info">
						<div class="center">
							<div class="jp-playlist">
								<ul>
									<li></li>
								</ul>
							</div>

						</div>


						<div class="progress jp-seek-bar">
							<span class="jp-play-bar" style="width: 0%"></span>
						</div>
					</div>

					<div class="controls">
						<div class="current jp-current-time">00:00</div>
						<div class="play-controls">
							<a href="javascript:;" class="icon-previous jp-previous" title="previous"></a>
							<a href="javascript:;" class="icon-play jp-play" title="play"></a>
							<a href="javascript:;" class="icon-pause jp-pause" title="pause"></a>
							<a href="javascript:;" class="icon-next jp-next" title="next"></a>
						</div>
						<div class="volume-level jp-volume-bar">
							<span class="jp-volume-bar-value" style="width: 0%"></span>
							<a href="javascript:;" class="icon-volume-up jp-volume-max" title="max volume"></a>
							<a href="javascript:;" class="icon-volume-down jp-mute" title="mute"></a>
						</div>
					</div>

					<div id="jquery_jplayer" class="jp-jplayer"></div>
				</div>
			</div>
			<div class="col-md-offset-2 col-md-4">
				<div class="page-header">
					<h2>友情链接</h2>
				</div>
				<ul class="list-unstyled">

					@foreach(\App\Link::all() as $link)
					<li><a href="{{$link->link}}" target="_blank">{{$link->name}}</a>
						@endforeach</li>						
					</ul>
					<div class="mt">
						<p>
							{!!$webset->webinfo!!}
						</p>
						<p>
							{!!$webset->copyright!!}
						</p>
						<p>
							{!!$webset->webcode!!}
						</p>
					</div>	
				</div>
			</div>
		</div>
</footer>
@endif

<script src="http://cdn.bootcss.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('public/js/suprise.js') }}"></script>
<script type="text/javascript" src="http://cdn.bootcss.com/scrollup/2.4.0/jquery.scrollUp.min.js"></script>
<script type="text/javascript">
$(function () {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '100', // Distance from top before showing element (px)
        topSpeed: 500, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // Animation in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: '<i class="glyphicon glyphicon-arrow-up"></i>', // Text for element
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        
    });
});
</script>
<script type="text/javascript">
     if(typeof(Worker) === "undefined"){
     	$.get('{{url("errorer")}}', function(data){
			$('body').html(data);
		})
     }
</script>
<script src="{{asset('public/player/js/jquery.jplayer.min.js')}}"></script>
<script src="{{asset('public/player/js/jplayer.playlist.min.js')}}"></script>
<script src="{{asset('public/player/js/index.js')}}"></script>
