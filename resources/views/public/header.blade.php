<link href="{{asset('public/css/timeline.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="http://cdn.bootcss.com/highlight.js/8.7/styles/railscasts.min.css">
<link rel="stylesheet" href="{{asset('public/player/css/reset.css')}}">
<link rel="stylesheet" href="http://cdn.bootcss.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="http://cdn.bootcss.com/font-awesome/4.2.0/css/font-awesome.min.css">

<link rel="stylesheet" href="{{asset('public/player/css/style.css')}}" media="screen" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{ asset('public/css/main.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('public/css/animate.css') }}">
<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
<script src="{{asset('public/player/js/pjax.js')}}"></script>
